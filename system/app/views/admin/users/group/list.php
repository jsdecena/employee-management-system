<div id="main_content" class="span9">
	<div class="bs-docs-example">
		<div class="controls">
			<h2>User Groups</h2>
			<div class="control-group">
				<p>These are the list of user groups.</p>
			</div>			
		</div>	

			<?php if ( $this->session->flashdata('success') ) : ?>
				<div class="alert-block alert-success">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>			
			<?php elseif ( $this->session->flashdata('error') ) : ?>
				<div class="alert-block alert-error">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<?php echo $this->session->flashdata('error') ?>
				</div>				
			<?php endif; ?>

			<div class="pull-left">
				<a href="<?php echo site_url('users/create_group');?>" class="btn btn-info">Create a group</a>
			</div>
			<table class="table table-striped <?php echo $this->uri->segment(2) ?>">
			  <thead>
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Description</th>
					<th>Actions</th>
				</tr>
			  </thead>
			  <tbody>
			  	<?php foreach ($groups as $group) : ?>
				  	<tr>
				  		<td><?php echo $group->id ?></td>
				  		<td><?php echo $group->name ?></td>
				  		<td><?php echo $group->description ?></td>
				  		<td>
				  			<a href="<?php echo site_url("users/edit_group/$group->id");?>" class="btn btn-primary">Edit</a>
				  			<a href="<?php echo site_url("users/delete_group/$group->id");?>" class="btn btn-danger" onClick="return confirm('Delete this group?')">Delete</a>
				  		</td>
				  	</tr>
			  	<?php endforeach; ?>
			  </tbody>
			</table>
			<p><?php echo $links; ?></p>
	</div>	
</div>