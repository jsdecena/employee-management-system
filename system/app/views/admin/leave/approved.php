<div id="main_content" class="span9">
	<div class="bs-docs-example">
		<div class="controls">
			<h2>Approved Leave Applications</h2>
			<div class="control-group">
				<p>These are the list of leave applications.</p>
			</div>			
		</div>
			<?php if ( $this->session->flashdata('success') ) : ?>
				<div class="alert-block alert-success">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>			
			<?php elseif ( $this->session->flashdata('error') ) : ?>
				<div class="alert-block alert-error">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<?php echo $this->session->flashdata('error') ?>
				</div>				
			<?php endif; ?>

			<?php if (is_array($leaves)) : ?>
			<table class="table table-striped <?php echo $this->uri->segment(2) ?>">
			  <thead>
				<tr>
					<th>ID</th>
					<th class="hidden-phone">Employee</th>
					<th>Type</th>
					<th>Timing</th>
					<th>Start Date</th>
					<th>End Date</th>
					<th>Shift</th>
					<th>Date</th>
					<th># of Days</th>
					<th>Admin</th>
				</tr>
			  </thead>
			  <tbody>
				<?php foreach ($leaves as $leave):?>
				  	<tr>
						<td><?php echo $leave->leave_id; ?></td>
						<td><?php echo $leave->first_name[0] .". ". $leave->last_name;?></td>
						<td><?php echo $leave->leave_type; ?></td>
						<td><?php echo $leave->leave_timing; ?></td>
						<td><?php echo date("F j, Y", strtotime($leave->start_date)); ?></td>
						<td><?php echo date("F j, Y", strtotime($leave->end_date)); ?></td>
						<td>
							<?php if ($leave->start_date == "0000-00-00 00:00:00") : ?>
								<?php echo $leave->shift; ?>
							<?php else: ?>
								-
							<?php endif; ?>
						</td>
						<td>
							<?php if ($leave->start_date == "0000-00-00 00:00:00") : ?>
								<?php echo date("F j, Y", strtotime($leave->half_date)); ?>
							<?php else: ?>
								-
							<?php endif; ?>							
						</td>						
						<td><?php echo $leave->no_days; ?></td>
						<td><?php echo $leave->admin; ?></td>
				  	</tr>
				<?php endforeach;?>
			  </tbody>
			</table>
			<?php else: ?>
				<section><p class="alert alert-error">You do not have approved leave application.</p></section>
			<?php endif; ?>			
			<p><?php echo $links; ?></p>
	</div>	
</div>