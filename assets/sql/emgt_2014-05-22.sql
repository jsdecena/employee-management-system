# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.5.29)
# Database: emgt
# Generation Time: 2014-05-22 18:41:23 +0800
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table cc_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cc_groups`;

CREATE TABLE `cc_groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `cc_groups` WRITE;
/*!40000 ALTER TABLE `cc_groups` DISABLE KEYS */;

INSERT INTO `cc_groups` (`id`, `name`, `description`)
VALUES
	(1,'admin','Administrator'),
	(2,'members','General User');

/*!40000 ALTER TABLE `cc_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cc_leaves
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cc_leaves`;

CREATE TABLE `cc_leaves` (
  `leave_id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `status` int(1) DEFAULT '0',
  `title` varchar(256) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `leave_type` varchar(256) DEFAULT NULL,
  `leave_timing` varchar(256) DEFAULT NULL,
  `shift` varchar(256) DEFAULT NULL,
  `date_applied` datetime DEFAULT '0000-00-00 00:00:00',
  `half_date` datetime DEFAULT '0000-00-00 00:00:00',
  `start_date` datetime DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime DEFAULT '0000-00-00 00:00:00',
  `no_days` int(8) DEFAULT NULL,
  `admin` varchar(256) DEFAULT NULL,
  `reason` text,
  `date_modified` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`leave_id`),
  KEY `fk_users_id` (`id`),
  CONSTRAINT `fk_userId` FOREIGN KEY (`id`) REFERENCES `cc_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table cc_login_attempts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cc_login_attempts`;

CREATE TABLE `cc_login_attempts` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table cc_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cc_sessions`;

CREATE TABLE `cc_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `cc_sessions` WRITE;
/*!40000 ALTER TABLE `cc_sessions` DISABLE KEYS */;

INSERT INTO `cc_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`)
VALUES
	('6e2f6799d57576a837cf28ab480b912c','127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36',1400755231,'a:7:{s:9:\"user_data\";s:0:\"\";s:8:\"identity\";s:15:\"admin@admin.com\";s:8:\"username\";s:13:\"administrator\";s:5:\"email\";s:15:\"admin@admin.com\";s:7:\"user_id\";s:1:\"1\";s:14:\"old_last_login\";s:10:\"1400755215\";s:12:\"is_logged_in\";i:1;}');

/*!40000 ALTER TABLE `cc_sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cc_settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cc_settings`;

CREATE TABLE `cc_settings` (
  `id_settings` int(11) NOT NULL AUTO_INCREMENT,
  `name_settings` varchar(256) DEFAULT NULL,
  `value_settings` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id_settings`),
  UNIQUE KEY `name_settings` (`name_settings`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `cc_settings` WRITE;
/*!40000 ALTER TABLE `cc_settings` DISABLE KEYS */;

INSERT INTO `cc_settings` (`id_settings`, `name_settings`, `value_settings`)
VALUES
	(1,'CURRENCY','Php'),
	(2,'BANK_ORIGIN','PHILIPPINES'),
	(3,'USERS_PER_PAGE','10'),
	(4,'SOFTWARE_NAME','Employee Management System'),
	(5,'ANNUAL_LEAVES','14'),
	(6,'MATERNITY_LEAVES','30'),
	(7,'SICK_LEAVES','7'),
	(8,'ADMIN_EMAIL','jeff.decena@yahoo.com'),
	(9,'ADMIN_NAME','Jeff Simons Decena');

/*!40000 ALTER TABLE `cc_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cc_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cc_users`;

CREATE TABLE `cc_users` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `prefix` char(5) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `nationality` varchar(50) DEFAULT NULL,
  `position` varchar(100) DEFAULT NULL,
  `employee_type` varchar(128) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `is_logged_in` int(11) unsigned NOT NULL DEFAULT '0',
  `street_addr` varchar(256) DEFAULT NULL,
  `town_city_addr` varchar(256) DEFAULT NULL,
  `post_code_addr` mediumint(8) unsigned DEFAULT NULL,
  `country_addr` varchar(256) DEFAULT NULL,
  `employment_date` date DEFAULT '0000-00-00',
  `m_phone` varchar(20) DEFAULT NULL,
  `monthly_salary` decimal(10,2) DEFAULT NULL,
  `bank_name` varchar(256) DEFAULT NULL,
  `acct_name` varchar(256) DEFAULT NULL,
  `acct_number` varchar(128) DEFAULT NULL,
  `others` text,
  `job_scope` text,
  `employee_currency` char(5) DEFAULT NULL,
  `annual_leaves` decimal(65,1) NOT NULL DEFAULT '14.0',
  `sick_leaves` decimal(65,1) NOT NULL DEFAULT '7.0',
  `maternity_leaves` decimal(65,1) NOT NULL DEFAULT '30.0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `cc_users` WRITE;
/*!40000 ALTER TABLE `cc_users` DISABLE KEYS */;

INSERT INTO `cc_users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `prefix`, `first_name`, `middle_name`, `last_name`, `nationality`, `position`, `employee_type`, `phone`, `is_logged_in`, `street_addr`, `town_city_addr`, `post_code_addr`, `country_addr`, `employment_date`, `m_phone`, `monthly_salary`, `bank_name`, `acct_name`, `acct_number`, `others`, `job_scope`, `employee_currency`, `annual_leaves`, `sick_leaves`, `maternity_leaves`)
VALUES
	(1,X'7F000001','administrator','514c09f43d5e94172e192865c19370cbe4352e8c','9462e8eee0','admin@admin.com',NULL,NULL,NULL,NULL,1268889823,1400755234,1,'Mr','Roland Jeffrey','Simons','Decena','Filipino','Web Developer','0','',0,'194 Poinsettia St.','Las Pinas City',1780,'Philippines','2010-10-04','+65-82514976',60000.00,'Banco de Oro - Makati Philippines','Roland Jeffrey S. Decena','001440034735','','Content Management System developer\nE-Commerce developer',NULL,10.5,4.0,0.0);

/*!40000 ALTER TABLE `cc_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cc_users_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cc_users_groups`;

CREATE TABLE `cc_users_groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `cc_users_groups` WRITE;
/*!40000 ALTER TABLE `cc_users_groups` DISABLE KEYS */;

INSERT INTO `cc_users_groups` (`id`, `user_id`, `group_id`)
VALUES
	(1,1,1),
	(10,9,2);

/*!40000 ALTER TABLE `cc_users_groups` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
