<div id="main_content" class="span9">
	<div class="bs-docs-example">
		<div class="controls">
			<h2>Leave Application</h2>
			<div class="control-group">
				<p>Approve or Deny Leave Applications.</p>
			</div>			
		</div>
			<?php if ( $this->session->flashdata('success') ) : ?>
				<div class="alert-block alert-success">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>			
			<?php elseif ( $this->session->flashdata('error') ) : ?>
				<div class="alert-block alert-error">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<?php echo $this->session->flashdata('error') ?>
				</div>				
			<?php endif; ?>

			<section>
				<div class="control-group">
					<label for="title">Title</label>
					<div class="controls">
						<p><?php echo $leave->title; ?></p>
					</div>
				</div>
				<div class="control-group">
					<label for="description">Description</label>
					<div class="controls">
						<p><?php echo $leave->description; ?></p>
					</div>
				</div>

				<div class="control-group">
					<label for="leave_type">Leave Type</label>
					<div class="controls">
						<p><?php echo $leave->leave_type; ?></p>
					</div>
				</div>

				<div class="control-group">
					<label for="leave_timing">Leave Timing</label>
					<div class="controls">
						<p><?php echo $leave->leave_timing; ?></p>
					</div>
				</div>

				<div class="control-group">
					<label for="date_applied">Applied date</label>
					<div class="controls">
						<p><?php echo date("F j, Y", strtotime($leave->date_applied)); ?></p>
					</div>
				</div>
				
				<?php if ($leave->start_date == "0000-00-00 00:00:00") : ?>

				<div class="control-group">
					<label for="shift">Shift</label>
					<div class="controls">
						<p><?php echo $leave->shift; ?></p>
					</div>
				</div>				
				
				<div class="control-group">
					<label for="half_date">Leave Date</label>
					<div class="controls">
						<p><?php echo date("F j, Y", strtotime($leave->half_date)); ?></p>
					</div>
				</div>
				
				<?php else: ?>

				<section>
					<div class="control-group pull-left">						
						<div class="controls">
							<label for="start_date">Start Date</label>
							<p><?php echo date("F j, Y", strtotime($leave->start_date)); ?></p>
						</div>
					</div>

					<div class="control-group pull-left">						
						<div class="controls">
							<label for="end_date">End Date</label>
							<p><?php echo date("F j, Y", strtotime($leave->end_date)); ?></p>
						</div>
					</div>					
				</section>

				<?php endif; ?>

				<div class="clearfix"></div>
				<?php echo form_open('leave/action'); ?>
					<div class="control-group">
						<label for="status">Action</label>
						<div class="controls">
							<select name="status" id="status">
								<option value="1">Approve</option>
								<option value="2">Deny</option>
							</select>
						</div>
					</div>				

					<div class="control-group">
						<label for="reason">Reason</label>
						<div class="controls">
							<textarea name="reason" id="reason" class="input-xlarge" cols="30" rows="5"></textarea>
						</div>
					</div>

					<div class="btn-group">
						<input type="hidden" name="leave_id" value="<?php echo $this->uri->segment(3); ?>">
						<input type="hidden" name="employee" value="<?php echo $employee; ?>">
						<button name="save" class="btn btn-primary">Save Action</button>
					</div>
				<?php echo form_close(); ?>

				<div class="clearfix"></div>
			</section>
	</div>	
</div>