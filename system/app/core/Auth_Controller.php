<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_Controller extends MY_Controller {

	function __construct() {

		parent::__construct();
	}

	//redirect if needed, otherwise display the user list
	public function index()
	{

		if (!$this->ion_auth->logged_in()) :
		
			//redirect them to the login page
			redirect('auth/login', 'refresh');
		else :
			
			redirect('auth/dashboard', 'refresh');
		endif;
	}

	public function dashboard()
	{

		if (!$this->ion_auth->logged_in()) :
		
			//redirect them to the login page
			redirect('auth/login', 'refresh');
		
		else :

	        // START DYNAMICALLY ADD JAVASCRIPTS
	        $css = array(	          
	            'http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css',
	            'assets/css/eventCalendar.css',
	            'assets/css/eventCalendar_theme_responsive.css'
	        );

			$this->template->stylesheet->add( $css );

	        // START DYNAMICALLY ADD JAVASCRIPTS
	        $js = array(	          
	            'assets/js/jquery-ui.js',
	            'assets/js/dashboard.js',
	            'assets/js/jquery.eventCalendar.js',
	            'assets/js/calendar.js'
	        );	        
	        $this->template->javascript->add($js);
	        // END DYNAMICALLY ADD JAVASCRIPTS

	        $this->template->title = 'Dashboard';	        

	    	$data['user'] 					= $this->ion_auth->user()->row();
	    	$data['hired_since'] 			= $this->tools_model->get_hiring_date($this->tools_model->fetch_hiring_date($data['user']->id));
	    	$data['s_name']					= $this->tools_model->get_settings('SOFTWARE_NAME');

	    	$data['annual_leaves_left']		= $this->tools_model->compute_annual_leaves($data['user']->id);
	    	$data['sick_leaves_left']		= $this->tools_model->compute_sick_leaves($data['user']->id);
	    	$data['maternity_leaves_left']	= $this->tools_model->compute_maternity_leaves($data['user']->id);

	        $this->template->content->view('admin/dashboard', $data);
	        
	        // publish the template
	        $this->template->publish();

    	endif;
	}

	//log the user in
	public function login()
	{
		$this->template->set_template('auth/login_tpl');

	    $this->template->title = 'Employee Management login';	    

		//validate form input
		$this->form_validation->set_rules('identity', 'Identity', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == true) :
		
			//check to see if the user is logging in
			//check for "remember me"
			$remember = (bool) $this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember)) :

				$data = array(
	                   'email'     		=> $this->input->post('identity'),
	                   'is_logged_in' 	=> 1
	            );

				$this->session->set_userdata($data);

				//if the login is successful
				//redirect them back to the home page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect('auth/dashboard', 'refresh');
			
			else :
			
				//if the login was un-successful
				//redirect them back to the login page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('auth/login', 'refresh'); //use redirects instead of loading views for compatibility with MY_Controller libraries
			
			endif;
		
		else :

			//the user is not logging in so display the login page
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['identity'] = array('name' => 'identity',
				'id' => 'identity',
				'type' => 'text',
				'value' => $this->form_validation->set_value('identity'),
			);
			$this->data['password'] = array('name' => 'password',
				'id' => 'password',
				'type' => 'password',
			);

			$this->template->content->view('auth/login', $this->data);			
		    
		    // publish the template
		    $this->template->publish();

		endif;
	}

	//log the user out
	public function logout()
	{
		$this->data['title'] = "Logout";

		//log the user out
		$logout = $this->ion_auth->logout();

		//redirect them to the login page
		$this->session->set_flashdata('message', $this->ion_auth->messages());
		redirect('auth/login', 'refresh');
	}

	//change password
	public function change_password()
	{
		$this->form_validation->set_rules('old', 'Old password', 'required');
		$this->form_validation->set_rules('new', 'New Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
		$this->form_validation->set_rules('new_confirm', 'Confirm New Password', 'required');

		if (!$this->ion_auth->logged_in()) :
		
			redirect('auth/login', 'refresh');
		
		endif;

		$user = $this->ion_auth->user()->row();

		if ($this->form_validation->run() == false) :
		
			//display the form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
			$this->data['old_password'] = array(
				'name' => 'old',
				'id'   => 'old',
				'type' => 'password',
			);
			$this->data['new_password'] = array(
				'name' => 'new',
				'id'   => 'new',
				'type' => 'password',
				'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
			);
			$this->data['new_password_confirm'] = array(
				'name' => 'new_confirm',
				'id'   => 'new_confirm',
				'type' => 'password',
				'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
			);
			$this->data['user_id'] = array(
				'name'  => 'user_id',
				'id'    => 'user_id',
				'type'  => 'hidden',
				'value' => $user->id,
			);

			//render
			$this->load->view('auth/change_password', $this->data);
		
		else :
		
			$identity = $this->session->userdata($this->config->item('identity', 'ion_auth'));

			$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

			if ($change) :
			
				//if the password was successfully changed
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				$this->logout();
			
			else :
			
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('auth/change_password', 'refresh');
			
			endif;
		
		endif;
	}

	//forgot password
	public function forgot_password()
	{
		$this->form_validation->set_rules('email', 'Email Address', 'required');
		
		if ($this->form_validation->run() == false) :
		
			//setup the input
			$this->data['email'] = array('name' => 'email',
				'id' => 'email',
			);

			//set any errors and display the form
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->load->view('auth/forgot_password', $this->data);
		
		else :
		
			//run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($this->input->post('email'));

			if ($forgotten) :
			
				//if there were no errors
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("auth/login", 'refresh'); //we should display a confirmation page here instead of the login page
			
			else :
			
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect("auth/forgot_password", 'refresh');
			
			endif;
		
		endif;
	}

	//reset password - final step for forgotten password
	public function reset_password($code = NULL)
	{
		if (!$code) :		
			show_404();
		endif;

		$user = $this->ion_auth->forgotten_password_check($code);

		if ($user) :
		  
			//if the code is valid then display the password reset form

			$this->form_validation->set_rules('new', 'New Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
			$this->form_validation->set_rules('new_confirm', 'Confirm New Password', 'required');

			if ($this->form_validation->run() == false) :
			
				//display the form

				//set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
				$this->data['new_password'] = array(
					'name' => 'new',
					'id'   => 'new',
					'type' => 'password',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				$this->data['new_password_confirm'] = array(
					'name' => 'new_confirm',
					'id'   => 'new_confirm',
					'type' => 'password',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				$this->data['user_id'] = array(
					'name'  => 'user_id',
					'id'    => 'user_id',
					'type'  => 'hidden',
					'value' => $user->id,
				);
				$this->data['csrf'] = $this->_get_csrf_nonce();
				$this->data['code'] = $code;

				//render
				$this->load->view('auth/reset_password', $this->data);
			
			else :
			
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id')) :
				

					//something fishy might be up
					$this->ion_auth->clear_forgotten_password_code($code);

					show_error('This form post did not pass our security checks.');

				
				else :
				
					// finally change the password
					$identity = $user->{$this->config->item('identity', 'ion_auth')};

					$change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

					if ($change) :
					
						//if the password was successfully changed
						$this->session->set_flashdata('message', $this->ion_auth->messages());
						$this->logout();
					
					else :
					
						$this->session->set_flashdata('message', $this->ion_auth->errors());
						redirect('auth/reset_password/' . $code, 'refresh');
					
					endif;
				
				endif;
			
			endif;
		
		else :
		
			//if the code is invalid then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("auth/forgot_password", 'refresh');
		
		endif;
	}

	//activate the user
	public function activate($id, $code=false)
	{
		if ($code !== false) :
		
			$activation = $this->ion_auth->activate($id, $code);
		
		elseif ($this->ion_auth->is_admin()) :
		
			$activation = $this->ion_auth->activate($id);
		
		endif;

		if ($activation) :
		
			//redirect them to the auth page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect('users/listing', 'refresh');
		
		else :
		
			//redirect them to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("auth/forgot_password", 'refresh');
		
		endif;
	}

	//deactivate the user
	public function deactivate($id = NULL)
	{

	    $this->template->title = 'Dashboard';		

		$id = $this->config->item('use_mongodb', 'ion_auth') ? (string) $id : (int) $id;

		$this->load->library('form_validation');
		$this->form_validation->set_rules('confirm', 'confirmation', 'required');
		$this->form_validation->set_rules('id', 'user ID', 'required|alpha_numeric');

		if ($this->form_validation->run() == FALSE) :
		
			// insert csrf check
			$this->data['csrf'] = $this->_get_csrf_nonce();
			$this->data['user'] = $this->ion_auth->user($id)->row();

			$this->template->content->view('admin/users/deactivate', $this->data);
	        
	        // publish the template
	        $this->template->publish();
		
		else :
		
			// do we really want to deactivate?
			if ($this->input->post('confirm') == 'yes') :
			
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id')) :
				
					show_error('This form post did not pass our security checks.');

				endif;

				// do we have the right userlevel?
				if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) :
				
					$this->ion_auth->deactivate($id);
				
				endif;
			
			endif;


			$this->session->set_flashdata('message', $this->ion_auth->messages());
								
			//redirect them back to the auth page
			redirect('users/listing', 'refresh');
		
		endif;
	}
}