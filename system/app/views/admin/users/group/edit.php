<div id="main_content" class="span9">
	<div class="bs-docs-example">
		<div class="controls">
			<h2>Edit a User Group</h2>
			<div class="control-group">
				<p>Edit a user group.</p>
			</div>			
		</div>

		<?php if ( $this->session->flashdata('success') ) : ?>
			<div class="alert-block alert-success">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<?php echo $this->session->flashdata('success') ?>
			</div>			
		<?php elseif ( $this->session->flashdata('error') ) : ?>
			<div class="alert-block alert-error">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<?php echo $this->session->flashdata('error') ?>
			</div>
		<?php endif; ?>

		<section class="control-group">
			<?php echo form_open("users/edit_group");?>
				<label for="name">Name <sup class="text-error">*</sup></label>
				<div class="controls">
					<input type="text" name="name" value="<?php echo $group->name ?>" />
				</div>
				<label for="description">Description <sup class="text-error">*</sup></label>
				<div class="controls">
					<textarea name="description" id="description" cols="30" rows="10" placeholder="Description"><?php echo $group->description ?></textarea>
				</div>
				<input type="hidden" name="id_group" value="<?php echo $group->id ?>" />
				<button class="btn btn-primary" name="edit_group">Save</button>
			<?php echo form_close(); ?>
		</section>
	</div>	
</div>