<ul class="nav nav-list bs-docs-sidenav affix-top">	
	<li <?php if ( $this->uri->segment(2) == 'dashboard') : ?> class="active"<?php endif; ?>> <a href="<?php echo base_url('auth/dashboard'); ?>"><i class="icon-chevron-right">&nbsp;</i> Dashboard</a></li>
	<li <?php if ( $this->uri->segment(2) == 'edit_my_profile') : ?> class="active"<?php endif; ?>><a href="<?php echo site_url('users/edit_my_profile');?>"><i class="icon-chevron-right">&nbsp;</i> My profile</a></li>
	<li <?php if ( $this->uri->segment(2) == 'apply') : ?> class="active"<?php endif; ?>><a href="<?php echo site_url('leave');?>"><i class="icon-chevron-right">&nbsp;</i> Apply a leave</a></li>	
</ul>
<?php if ( $this->ion_auth->is_admin() ) : ?>
	<ul class="nav nav-list bs-docs-sidenav affix-top">
		<li> <a href="javascript: void(0)"> <strong>Leave Controls</strong> </a></li>
		<li <?php if ( $this->uri->segment(1) == 'leave' && $this->uri->segment(2) == 'pending' ) : ?> class="active"<?php endif; ?>><a href="<?php echo site_url('leave/pending');?>"><i class="icon-chevron-right">&nbsp;</i> Pending applications</a></li>
		<li <?php if ( $this->uri->segment(1) == 'leave' && $this->uri->segment(2) == 'approved' ) : ?> class="active"<?php endif; ?>><a href="<?php echo site_url('leave/approved');?>"><i class="icon-chevron-right">&nbsp;</i> Approved applications</a></li>
		<li <?php if ( $this->uri->segment(1) == 'leave' && $this->uri->segment(2) == 'denied' ) : ?> class="active"<?php endif; ?>><a href="<?php echo site_url('leave/denied');?>"><i class="icon-chevron-right">&nbsp;</i> Denied applications</a></li>
	</ul>

	<ul class="nav nav-list bs-docs-sidenav affix-top">
		<li> <a href="javascript: void(0)"> <strong>Admin Control</strong> </a></li>			
		<li <?php if ( $this->uri->segment(2) == 'listing' || $this->uri->segment(2) == 'edit' || $this->uri->segment(2) == 'deactivate' || $this->uri->segment(2) == 'create') : ?> class="active"<?php endif; ?>><a href="<?php echo site_url('users/listing');?>"><i class="icon-chevron-right">&nbsp;</i> Users list</a></li>
		<li <?php if ( $this->uri->segment(1) == 'users' && $this->uri->segment(2) == 'group' || $this->uri->segment(2) == "edit_group" || $this->uri->segment(2) == "create_group" ) : ?> class="active"<?php endif; ?>><a href="<?php echo site_url('users/group');?>"><i class="icon-chevron-right">&nbsp;</i> Groups</a></li>
		<li <?php if ( $this->uri->segment(1) == 'settings') : ?> class="active"<?php endif; ?>><a href="<?php echo site_url('settings');?>"><i class="icon-chevron-right">&nbsp;</i> Software settings</a></li>				
	</ul>
<?php endif; ?>