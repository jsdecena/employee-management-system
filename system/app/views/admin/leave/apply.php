<?php if ( isset($user)) : ?>
	<div id="main_content" class="span9">
		<section class="bs-docs-example">
			
			<legend>Apply a Leave</legend>

			<?php if ( $this->session->flashdata('success') ) : ?>
				<div class="alert-block alert-success">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>			
			<?php elseif ( $this->session->flashdata('error') ) : ?>
				<div class="alert-block alert-error">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<?php echo $this->session->flashdata('error') ?>
				</div>				
			<?php endif; ?>

			<?php echo form_open('leave/apply', 'id="leave_apply_form"'); ?>

			<section class="control-group">
				
				<label>Select type of leave to apply <sup class="text-error">*</sup></label>
				<div class="control-group">
					<?php echo form_dropdown('leave_type', $leave_type, $this->input->post('leave_type')); ?>
				</div>

				<label>Select leave timing <sup class="text-error">*</sup></label>
				<div class="control-group">
					<?php echo form_dropdown('leave_timing', $leave_timing, 0 ? $this->input->post('leave_timing') : '', 'id="leave_timing"' ); ?>
				</div>

				<div class="control-group half_day_option" style="display:none">
					<label>Select shift <sup class="text-error">*</sup></label>
					<div>
						<?php echo form_dropdown('shift', $shift, 0 ? $this->input->post('shift') : '', 'id="shift"' ); ?>
					</div>

					<div class="input-append">
						<label for="date">Date: </label>
						<div class="input-prepend datepicker">
							<span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
							<?php echo form_input($half_date);?>
						</div>
					</div>										
				</div>				

				<label>Title: <sup class="text-error">*</sup> </label>
				<div class="control-group">
					<?php echo form_input($title); ?>
				</div>

				<label>Description:</label>
				<div class="control-group">
					<?php echo form_textarea($description); ?>
				</div>

				<div class="full_day_option">
					<div class="span4 pull-left">
						<div class="input-append">
							<label for="start_date">Start Date: </label>
							<div class="input-prepend dtpicker">
								<span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
								<?php echo form_input($start_date);?>
							</div>
						</div>						
					</div>					
					<div class="span4 pull-left">				
						<div class="input-append">
							<label for="end_date">End Date: </label>
							<div class="input-prepend dtpicker">
								<span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
								<?php echo form_input($end_date);?>
							</div>
						</div>						
					</div>											
				</div>

				<div class="form_btn_wrap">
					<?php echo form_input($id_user, ''); ?>
					<?php echo form_submit($apply);?>
				</div>
			</section>

			<?php echo form_close(); ?>

			<div class="control-group">

				<?php echo $this->load->view('admin/calendar'); ?>

				<div class="controls pull-left" style="display:none">
					<label for="date_from">From: </label>
					<div class="input-append">
					  <input class="span2" id="date_from" type="text">
					  <span class="add-on"><i class="icon-calendar">&nbsp;</i></span>
					</div>			
				</div>

				<div class="controls pull-left" style="display:none">
					<label for="date_to">To: </label>
					<div class="input-append">
					  <input class="span2" id="date_to" type="text">
					  <span class="add-on"><i class="icon-calendar">&nbsp;</i></span>
					</div>			
				</div>								

			</div>			
		</section>
	</div>
<?php endif; ?>