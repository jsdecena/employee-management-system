<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tools_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

	public function get_settings($name_setting_value)
    {
        $this->db->select('value_settings')->from('settings')->where('name_settings', $name_setting_value);
        $query = $this->db->get();

		foreach ($query->result_array() as $row) {
		    return $row['value_settings'];
		}
	}

	public function set_currency($name_setting_value)
    {

		$data = array( 'value_settings' => $_POST['set_currency'] );

		$this->db->where('name_settings', $name_setting_value);
		$this->db->update('settings', $data); 
	}

	public function set_users_per_page($name_setting_value)
    {

		$data = array( 'value_settings' => $_POST['users_per_page'] );

		$this->db->where('name_settings', $name_setting_value);
		$this->db->update('settings', $data); 
	}

    public function set_software_name($name_setting_value)
    {

        $data = array( 'value_settings' => $_POST['software_name'] );

        $this->db->where('name_settings', $name_setting_value);
        $this->db->update('settings', $data);

    }

    public function set_annual_leaves($name_setting_value)
    {

        $data = array( 'value_settings' => $_POST['annual_leaves'] );

        $this->db->where('name_settings', $name_setting_value);
        $this->db->update('settings', $data);

    }

    public function set_sick_leaves($name_setting_value)
    {

        $data = array( 'value_settings' => $_POST['sick_leaves'] );

        $this->db->where('name_settings', $name_setting_value);
        $this->db->update('settings', $data);

    } 

    public function set_maternity_leaves($name_setting_value)
    {

        $data = array( 'value_settings' => $_POST['maternity_leaves'] );

        $this->db->where('name_settings', $name_setting_value);
        $this->db->update('settings', $data);

    }

   //COMPUTE THE HIRING DATE BASED ON THE POSTED DATA
    public function get_hiring_date($hiring_date)
    {

        $date1 = date('Y-m-d');
        $date2 = $hiring_date;

        $diff = abs(strtotime($date2) - strtotime($date1));

        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

        $hired_since = sprintf("%d year(s) %d month(s) %d day(s)\n", $years, $months, $days);

        return $hired_since;
    }

    //GET THE HIRING DATE FROM THE DB
    public function fetch_hiring_date( $id_employee = 1 )
    {

        $this->db->select('employment_date')->from('users')->where('id', $id_employee);
        $query = $this->db->get();

        foreach ($query->result() as $value) {
            return $value->employment_date;
        }
    }

    public function compute_annual_leaves($id_user)
    {
        //GET THE DEFAULT ANNUAL LEAVE IN THE SETTINGS TABLE
        $default_annual_leave   = $this->get_settings('annual_leaves');

        //GET THE ANNUAL LEAVE OF THE USER IN THE USERS TABLE
        $this->db->select('annual_leaves')->from('users')->where('id', $id_user);        
            $users_leaves = $this->db->get();

            foreach ($users_leaves->result() as $row) {
                
                $ul = $row->annual_leaves;        

            }

        //GET THE PERCENTAGE
        //@param ul_leave_percentage = users_leave_percentage
        $ul_leave_percentage = ( $ul / $default_annual_leave ) * 100;
        $ul_leave_percentage = round($ul_leave_percentage, 2);

        return $ul_leave_percentage;
    }

    public function compute_sick_leaves($id_user)
    {

        //GET THE DEFAULT ANNUAL LEAVE IN THE SETTINGS TABLE
        $default_sick_leave   = $this->get_settings('sick_leaves');

        //GET THE SICK LEAVE OF THE USER IN THE USERS TABLE
        $this->db->select('sick_leaves')->from('users')->where('id', $id_user);        
            $users_leaves = $this->db->get();

            foreach ($users_leaves->result() as $row) {
                
                $ul = $row->sick_leaves;        

            }

        //GET THE PERCENTAGE
        //@param ul_leave_percentage = users_leave_percentage
        $ul_leave_percentage = ( $ul / $default_sick_leave ) * 100;
        $ul_leave_percentage = round($ul_leave_percentage, 2);

        return $ul_leave_percentage;
    }

    public function compute_maternity_leaves($id_user)
    {

        //GET THE DEFAULT ANNUAL LEAVE IN THE SETTINGS TABLE
        $default_sick_leave   = $this->get_settings('maternity_leaves');

        //GET THE MATERNITY LEAVE OF THE USER IN THE USERS TABLE
        $this->db->select('maternity_leaves')->from('users')->where('id', $id_user);    
            $users_leaves = $this->db->get();

            foreach ($users_leaves->result() as $row) {
                
                $ul = $row->maternity_leaves;        

            }

        //GET THE PERCENTAGE
        //@param ul_leave_percentage = users_leave_percentage
        $ul_leave_percentage = ( $ul / $default_sick_leave ) * 100;
        $ul_leave_percentage = round($ul_leave_percentage, 2);

        return $ul_leave_percentage;
    }
}