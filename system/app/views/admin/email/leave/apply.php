<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Leave Application</title>
    </head>
    <body>
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
            <tr>
                <td align="center" valign="top">
                    <table border="0" cellpadding="20" cellspacing="0" width="600" id="emailContainer">
                        <tr>
                            <td align="left" valign="top"> 
                                <?php $half_day = (bool)$_POST['half_date']; ?>
                                Hi Admin, <br /> <br />
                                Employee <?php echo $_POST['name']; ?> has filed a leave with you. <br /> <br />
                                Here are the details : <br />                                
                                Title : <?php echo $_POST['title']; ?> <br />
                                Description : <?php echo $_POST['description']; ?> <br />
                                Leave type : <?php echo $_POST['leave_type']; ?> <br />
                                
                                <?php if ($half_day !== true ) : ?>
                                    Date start : <?php echo date("F j, Y", strtotime($_POST['start_date'])); ?><br />
                                    Date end : <?php echo date("F j, Y", strtotime($_POST['end_date'])); ?>
                                <?php else: ?>
                                    Date : <?php echo date("F j, Y", strtotime($_POST['half_date'])); ?><br />
                                    Shift : <?php echo $_POST['shift']; ?><br />
                                <?php endif; ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>