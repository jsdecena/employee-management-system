$(document).ready(function()
{
	//TAB SCRIPT
	$('#myTab a').on('click',function(e) 
	{
	  e.preventDefault();
	  $(this).tab('show');
	});

	if( $('.datepicker').length > 0 || $('.dtpicker').length > 0) 
	{		
		$('.datepicker').datetimepicker({ language: 'en', pickTime: false });
		$('.dtpicker').datetimepicker({ language: 'en', format: 'yyyy-MM-dd hh:mm:ss' });
	}

	fadeInOut("#leave_timing", ".half_day_option", ".full_day_option", "Half Day");

	//TOGGLE EDIT PROFILE
	toggleTab("a.edit_prof_btn", "#employee_profile_show", ".profile");

	//TOGGLE EDIT ADDRESS
	toggleTab("a.edit_add_btn", "#show_address", ".address");

});

/*CLICK TOGGLE*/
var clicked = false;

function toggleTab( btn, tabHide, tabShow ){

	if (btn.length > 0) {
		//EMPLOYEE PROFILE EDIT TOGGLE
		$(btn).click(function(){

			if ( clicked == true ) {

				clicked = false;

				//ADDRESS EDIT BTN
				$(this).removeClass('btn-warning').addClass('btn-info').text('Edit');
				$(tabHide).fadeIn('fast');
				$(tabShow).fadeOut('slow').addClass('hidden');		
			
			}else{

				clicked = true;

				//ADDRESS EDIT BTN
				$(this).removeClass('btn-info').addClass('btn-warning').text('Cancel');
				$(tabHide).fadeOut('slow');
				$(tabShow).fadeIn('fast').removeClass('hidden');
			}

			return false;
		});
	};

}
/*END CLICK TOGGLE*/

/*FADE-IN/OUT*/
function fadeInOut(btn, fadeIn, fadeOut, condition)
{
	if (btn.length > 0) {
		//IF USER PICKS HALF DAY
		$(btn).on('change', function()
		{
			if( $(this).val() == condition)
			{

				//FADEOUT THE FULL DAY OPTIONS
				$(fadeOut).fadeOut();

				//FADEIN THE HALF DAY OPTION
				$(fadeIn).fadeIn().removeClass('hidden');
			}else{

				//FADEIN THE FULL DAY OPTIONS
				$(fadeOut).fadeIn();

				//FADEOUT THE HALF DAY OPTION
				$(fadeIn).fadeOut().addClass('hidden');		
			}
		});
	};	
}
/*END FADE-IN/OUT*/