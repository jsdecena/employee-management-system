<div id="main_content" class="span9">
	<div class="bs-docs-example">
		<div class="controls">
			<h2>Create a User Group</h2>
			<div class="control-group">
				<p>Create a user group.</p>
			</div>			
		</div>

		<?php if ( $this->session->flashdata('success') ) : ?>
			<div class="alert-block alert-success">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<?php echo $this->session->flashdata('success') ?>
			</div>			
		<?php elseif ( $this->session->flashdata('error') ) : ?>
			<div class="alert-block alert-error">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<?php echo $this->session->flashdata('error') ?>
			</div>				
		<?php endif; ?>

		<section class="control-group">
			<?php echo form_open("users/create_group");?>
				<label for="name">Name</label>
				<div class="controls">
					<input type="text" name="name" value="<?php $this->input->post('name')?$this->input->post('name'):null ?>" placeholder="Group Name" />
				</div>
				<label for="description">Description</label>
				<div class="controls">
					<textarea name="description" id="description" cols="30" rows="10" placeholder="Description"><?php $this->input->post('description')?$this->input->post('description'):null ?></textarea>
				</div>
				<button class="btn btn-primary" name="create_group">Create</button>
			<?php echo form_close(); ?>
		</section>
	</div>	
</div>