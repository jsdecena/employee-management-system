<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model
{
   public function fetch($table, $limit=10, $start=0)
   {
        $this->db->limit($limit, $start);
        $query = $this->db->get($table);

        $data = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
   }

   public function fetch_where($table, $column, $where)
   {
   		$this->db->where($column, $where);
        $query = $this->db->get($table);

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data = $row;
            }
            return $data;
        }
   }

   public function fetch_where_array($table, $column, $where)
   {
        $this->db->where($column, $where);
        $query = $this->db->get($table);

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
   }

   public function get_record_count($table)
   {
        $this->db->from($table);
        $query = $this->db->get();
        $rowcount = $query->num_rows();

        return $rowcount;
   }

   public function insert_record($table, $records = array())
   {
        $this->db->insert($table, $records);
   }

   public function update_record($table, $column, $where, $data = array())
   {
		$this->db->where($column, $where);
		$this->db->update($table, $data);    	
   }

   public function delete_record($table, $records = array())
   {
		$this->db->delete($table, $records);
   }

   public function get_users_groups()
   {
   		$query = $this->db->query('
			SELECT u.email, u.first_name, u.last_name, u.id AS user_id, ug.id AS user_group_id, g.*
			FROM cc_groups AS g
			JOIN cc_users_groups AS ug ON g.id = ug.group_id
			JOIN cc_users AS u ON ug.user_id = u.id
   		');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

   public function fetch_groups($table)
   {
   		$query = $this->db->get($table);

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
            return $data;
        } 
   }
}