<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_Controller extends MY_Controller
{
    function __construct() 
    {
        parent::__construct();
        $this->load->model('users_model');
    }

    public function index()
    {

        if (!$this->ion_auth->logged_in()) :
            //redirect them to the login page
            redirect('auth/login', 'refresh');
        else :
            redirect('users/listing', 'refresh');
        endif;
    }

    public function listing()
    {

        if (!$this->ion_auth->logged_in()) :
        
            //redirect them to the login page
            redirect('auth/login', 'refresh');
        
        elseif (!$this->ion_auth->is_admin()) :
        
            //redirect them to the home page because they must be an administrator to view this
            redirect('users/index', 'refresh');
        
        else :

            $this->template->title = 'Dashboard';

            $this->load->library('pagination');

            $config                 = array();
            $config["base_url"]     = base_url('users/listing');
            $config["total_rows"]   = $this->users_model->get_record_count('users');
            $config["per_page"]     = $this->tools_model->get_settings('USERS_PER_PAGE');
            $config["uri_segment"]  = 3;

            //ADDITIONAL PAGINATION STYLING
            $config['full_tag_open']    = '<div class="pagination"><ul>';
            $config['full_tag_close']   = '</ul></div>';
            $config['cur_tag_open']     = '<li class="active"><a href="javascript:void(0)">';
            $config['cur_tag_close']    = '</a></li>';
            $config['num_tag_open']     = '<li>';
            $config['num_tag_close']    = '</li>';
            $config['last_tag_open']    = '<li>';
            $config['last_tag_close']   = '</li>';
            $config['next_tag_open']    = '<li>';
            $config['next_tag_close']   = '</li>';
            $config['prev_tag_open']    = '<li>';
            $config['prev_tag_close']   = '<li>';

            $this->pagination->initialize($config);

            $page                   = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $this->data["users"]    = $this->users_model->fetch("users", $config["per_page"], $page);
            $this->data["links"]    = $this->pagination->create_links();

            //set the flash data error message if there is one
            $this->data['message']  = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            //list the users            
            foreach ($this->data['users'] as $k => $user)
            {
                $this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
            }

            $this->data['s_name'] = $this->tools_model->get_settings('SOFTWARE_NAME');            

            $this->template->content->view('admin/users/listing', $this->data);
            
            // publish the template
            $this->template->publish();
        endif;
    }

    //CREATE A USER
    public function create()
    {
        $this->template->title                  = 'User Create';
        $this->data['s_name']                   = $this->tools_model->get_settings('SOFTWARE_NAME');

        $this->template->javascript->add('assets/js/myscript.js');

        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) :        
            redirect('auth', 'refresh');
        endif;

        //validate form input
        $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');
        $this->form_validation->set_rules('position','Position','required|callback_check_default');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', 'Password Confirmation', 'required');

        $this->form_validation->set_message('check_default', 'You must select a company position for the user.');

        if ($this->form_validation->run() == true) :
        
            $username = strtolower($this->input->post('first_name')) . ' ' . strtolower($this->input->post('last_name'));
            $email    = $this->input->post('email');
            $password = $this->input->post('password');

            $additional_data = array(
                'prefix'            => $this->input->post('prefix'),
                'first_name'        => $this->input->post('first_name'),
                'middle_name'       => $this->input->post('middle_name'),
                'last_name'         => $this->input->post('last_name'),
                'nationality'       => $this->input->post('nationality'),
                'position'          => $this->input->post('position'),
                'employee_type'     => $this->input->post('employee_type'),
                'job_scope'         => $this->input->post('job_scope'),
                'email'             => $this->input->post('email'),
                'street_addr'       => $this->input->post('street_addr'),
                'town_city_addr'    => $this->input->post('town_city_addr'),
                'post_code_addr'    => $this->input->post('post_code_addr'),
                'country_addr'      => $this->input->post('country_addr'),
                'phone'             => $this->input->post('phone'),
                'm_phone'           => $this->input->post('m_phone'),
                'employment_date'   => $this->input->post('employment_date'),
                'monthly_salary'    => number_format($this->input->post('monthly_salary', 2, '.', '')),
                'bank_name'         => $this->input->post('bank_name'),
                'acct_name'         => $this->input->post('acct_name'),
                'acct_number'       => $this->input->post('acct_number'),
                'others'            => $this->input->post('others')         
            );
        endif;

        if ($this->form_validation->run() == true && $this->ion_auth->register($username, $password, $email, $additional_data)) :

            $user = $this->ion_auth->user($id)->row();

            $this->session->set_flashdata('error', $this->ion_auth->messages());
            redirect("users/listing", 'refresh');
        else :
        
            //display the create user form
            //set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $this->data['prefix'] = array(
                'name'  => 'prefix',
                'id'    => 'prefix',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('prefix', $this->input->post('prefix'))
            );
            $this->data['first_name'] = array(
                'name'  => 'first_name',
                'id'    => 'first_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('first_name', $this->input->post('first_name'))
            );
            $this->data['middle_name'] = array(
                'name'  => 'middle_name',
                'id'    => 'middle_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('middle_name', $this->input->post('middle_name'))
            );          
            $this->data['last_name'] = array(
                'name'  => 'last_name',
                'id'    => 'last_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('last_name', $this->input->post('last_name'))
            );
            $this->data['nationality'] = array(
                'name'  => 'nationality',
                'id'    => 'nationality',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('nationality', $this->input->post('nationality'))
            );          
            $this->data['street_addr'] = array(
                'name'  => 'street_addr',
                'id'    => 'street_addr',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('street_addr', $this->input->post('street_addr'))
            );
            $this->data['town_city_addr'] = array(
                'name'  => 'town_city_addr',
                'id'    => 'town_city_addr',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('town_city_addr', $this->input->post('town_city_addr'))
            );
            $this->data['post_code_addr'] = array(
                'name'  => 'post_code_addr',
                'id'    => 'post_code_addr',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('post_code_addr', $this->input->post('post_code_addr'))
            );
            $this->data['country_addr'] = array(
                'name'  => 'country_addr',
                'id'    => 'country_addr',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('country_addr', $this->input->post('country_addr'))
            );                                      
            $this->data['email'] = array(
                'name'  => 'email',
                'id'    => 'email',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('email', $this->input->post('email'))
            );
            $this->data['position'] = array(
                'name'  => 'position',
                'id'    => 'position',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('position', $this->input->post('position'))
            );          
            $this->data['job_scope'] = array(
                'name'  => 'job_scope',
                'id'    => 'job_scope',
                'value' => $this->form_validation->set_value('job_scope', $this->input->post('job_scope'))
            );          
            $this->data['phone'] = array(
                'name'  => 'phone',
                'id'    => 'phone',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('phone', $this->input->post('phone'))
            );
            $this->data['m_phone'] = array(
                'name'  => 'm_phone',
                'id'    => 'm_phone',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('m_phone', $this->input->post('m_phone'))
            );
            $this->data['employment_date'] = array(
                'data-format' => 'yyyy-mm-dd',
                'name'  => 'employment_date',
                'id'    => 'employment_date',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('employment_date', $this->input->post('employment_date'))
            );          
            $this->data['monthly_salary'] = array(
                'name'  => 'monthly_salary',
                'id'    => 'monthly_salary',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('monthly_salary', number_format($this->input->post('monthly_salary', 2, '.', '')) )
            );

            //BANK DETAILS
            $this->data['bank_name'] = array(
                'name'  => 'bank_name',
                'id'    => 'bank_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('bank_name', $this->input->post('bank_name'))
            );
            $this->data['acct_name'] = array(
                'name'  => 'acct_name',
                'id'    => 'acct_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('acct_name', $this->input->post('acct_name'))
            );
            $this->data['acct_number'] = array(
                'name'  => 'acct_number',
                'id'    => 'acct_number',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('acct_number', $this->input->post('acct_number'))
            );
            $this->data['others'] = array(
                'name'  => 'others',
                'id'    => 'others',
                'value' => $this->form_validation->set_value('others', $this->input->post('others'))
            );  
            $this->data['password'] = array(
                'name'  => 'password',
                'id'    => 'password',
                'type'  => 'password',
                'value' => $this->form_validation->set_value('password')
            );
            $this->data['password_confirm'] = array(
                'name'  => 'password_confirm',
                'id'    => 'password_confirm',
                'type'  => 'password',
                'value' => $this->form_validation->set_value('password_confirm')
            );


            //GET THE USER GROUPS
            $this->data["groups"]       = $this->users_model->fetch("groups");
            $this->data["ug"]           = $this->users_model->fetch("users_groups");

            //ITERATE ON THE USERS_GROUP TABLE
            foreach ($this->data["ug"] as $group) :
                if ($group->user_id == $user->id) :
                    
                    //GET THE GROUP ID OF THE CURRENT USER
                    $this->data['g_id']   = $group->group_id;
                endif;
            endforeach;

            $this->template->content->view('admin/users/create', $this->data);
            
            // publish the template
            $this->template->publish();
        endif;
    }

    //EDIT A USER
    public function edit($id)
    {
        
        $this->template->title                  = 'User Edit';

        $this->template->javascript->add('assets/js/myscript.js');     

        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth', 'refresh');
        }

        $user = $this->ion_auth->user($id)->row();

        //validate form input
        $this->form_validation->set_rules('first_name', 'First Name', 'required|xss_clean');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required|xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('position','Position','required|callback_check_default');

        $this->form_validation->set_message('check_default', 'You must select a company position for the user.');

        if ($this->input->post('save_user'))
        {
            $data = array(
                'prefix'            => $this->input->post('prefix'),
                'first_name'        => $this->input->post('first_name'),
                'middle_name'       => $this->input->post('middle_name'),
                'last_name'         => $this->input->post('last_name'),
                'nationality'       => $this->input->post('nationality'),
                'position'          => $this->input->post('position'),
                'employee_type'     => $this->input->post('employee_type'),
                'job_scope'         => $this->input->post('job_scope'),
                'email'             => $this->input->post('email'),
                'street_addr'       => $this->input->post('street_addr'),
                'town_city_addr'    => $this->input->post('town_city_addr'),
                'post_code_addr'    => $this->input->post('post_code_addr'),
                'country_addr'      => $this->input->post('country_addr'),
                'phone'             => $this->input->post('phone'),
                'm_phone'           => $this->input->post('m_phone'),
                'annual_leaves'     => $this->input->post('annual_leaves'),
                'sick_leaves'       => $this->input->post('sick_leaves'),
                'maternity_leaves'  => $this->input->post('maternity_leaves'),
                'monthly_salary'    => $this->input->post('monthly_salary'),
                'bank_name'         => $this->input->post('bank_name'),
                'acct_name'         => $this->input->post('acct_name'),
                'acct_number'       => $this->input->post('acct_number'),
                'others'            => $this->input->post('others')         
            );

            //update the password if it was posted
            if ($this->input->post('password')) :
            
                $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
                $this->form_validation->set_rules('password_confirm', 'Password Confirmation', 'required');

                $data['password'] = $this->input->post('password');
            
            endif;

            if ( $this->form_validation->run() ) :
            
                //UPDATE THE USER TABLE
                $this->ion_auth->update($user->id, $data);

                $data = array('group_id' => $this->input->post('group'));

                //UPDATE THE USERS GROUP TABLE
                $this->users_model->update_record("users_groups", "user_id", $this->input->post('id'), $data);

                $this->session->set_flashdata('message', "Edited successfully");
                redirect("users/edit/$id", 'refresh');
            
            endif;

        }elseif ($this->input->post('delete_user')) {
            
            // do we have the right userlevel?
            if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) :

                $this->ion_auth->delete_user($_POST['id']);
            
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                
                //redirect them back to the auth page
                redirect('users/listing', 'refresh');

            else:

                $this->session->set_flashdata('message', $this->ion_auth->messages());
            
                //redirect them back to the auth page
                redirect('users/listing', 'refresh');
            
            endif;
        }   
        
        //display the edit user form
        $this->data['csrf'] = $this->_get_csrf_nonce();

        //set the flash data error message if there is one
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

        //pass the user to the view
        $this->data['user'] = $user;

        $this->data['prefix'] = array(
            'name'  => 'prefix',
            'id'    => 'prefix',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('prefix', $user->prefix)
        );
        $this->data['first_name'] = array(
            'name'  => 'first_name',
            'id'    => 'first_name',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('first_name', $user->first_name)
        );
        $this->data['middle_name'] = array(
            'name'  => 'middle_name',
            'id'    => 'middle_name',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('middle_name', $user->middle_name)
        );      
        $this->data['last_name'] = array(
            'name'  => 'last_name',
            'id'    => 'last_name',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('last_name', $user->last_name)
        );
        $this->data['nationality'] = array(
            'name'  => 'nationality',
            'id'    => 'nationality',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('nationality', $user->nationality)
        );      
        $this->data['street_addr'] = array(
            'name'  => 'street_addr',
            'id'    => 'street_addr',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('street_addr', $user->street_addr)
        );
        $this->data['town_city_addr'] = array(
            'name'  => 'town_city_addr',
            'id'    => 'town_city_addr',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('town_city_addr', $user->town_city_addr)
        );
        $this->data['post_code_addr'] = array(
            'name'  => 'post_code_addr',
            'id'    => 'post_code_addr',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('post_code_addr', $user->post_code_addr)
        );
        $this->data['country_addr'] = array(
            'name'  => 'country_addr',
            'id'    => 'country_addr',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('country_addr', $user->country_addr)
        );
        $this->data['email'] = array(
            'name'  => 'email',
            'id'    => 'email',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('email', $user->email, $user->email)
        );      
        $this->data['position'] = array(
            'name'  => 'position',
            'id'    => 'position',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('position', $user->position)
        );
        $data['employee_type'] = array(
            '0' => 'Select',
            'Full-time' => 'Full-time Employee',
            'Part-time' => 'Part-time Employee'
        );  
        $this->data['job_scope'] = array(
            'name'  => 'job_scope',
            'id'    => 'job_scope',
            'value' => $this->form_validation->set_value('job_scope', $user->job_scope)
        );      
        $this->data['phone'] = array(
            'name'  => 'phone',
            'id'    => 'phone',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('phone', $user->phone)
        );
        $this->data['m_phone'] = array(
            'name'  => 'm_phone',
            'id'    => 'm_phone',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('m_phone', $user->m_phone)
        );
        $this->data['annual_leaves'] = array(
            'name'  => 'annual_leaves',
            'id'    => 'annual_leaves',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('annual_leaves', $user->annual_leaves)
        );
        $this->data['sick_leaves'] = array(
            'name'  => 'sick_leaves',
            'id'    => 'sick_leaves',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('sick_leaves', $user->sick_leaves)
        );
        $this->data['maternity_leaves'] = array(
            'name'  => 'maternity_leaves',
            'id'    => 'maternity_leaves',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('maternity_leaves', $user->maternity_leaves)
        );              
        $this->data['employment_date'] = array(
            'data-format' => 'yyyy-mm-dd',
            'name'  => 'employment_date',
            'id'    => 'employment_date',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('employment_date', $user->employment_date)
        );

        $this->data['monthly_salary'] = array(
            'name'  => 'monthly_salary',
            'id'    => 'monthly_salary',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('monthly_salary', number_format($user->monthly_salary, 2, '.', ''))
        );

        //BANK DETAILS
        $this->data['bank_name'] = array(
            'name'  => 'bank_name',
            'id'    => 'bank_name',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('bank_name', $user->bank_name)
        );
        $this->data['acct_name'] = array(
            'name'  => 'acct_name',
            'id'    => 'acct_name',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('acct_name', $user->acct_name)
        );
        $this->data['acct_number'] = array(
            'name'  => 'acct_number',
            'id'    => 'acct_number',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('acct_number', $user->acct_number)
        );      
        $this->data['others'] = array(
            'name'  => 'others',
            'id'    => 'others',
            'value' => $this->form_validation->set_value('others', $user->others)
        );
        //SAVING A USER     
        $this->data['password'] = array(
            'name' => 'password',
            'id'   => 'password',
            'type' => 'password'
        );
        $this->data['password_confirm'] = array(
            'name' => 'password_confirm',
            'id'   => 'password_confirm',
            'type' => 'password'
        );

        $this->data['hired_since'] = array(
            'name' => 'hired_since',
            'id'   => 'hired_since',
            'type' => 'text',
            'value' => $this->form_validation->set_value('hired_since', $this->tools_model->get_hiring_date($user->employment_date))
        );

        $this->data['default_currency'] = $this->tools_model->get_settings('CURRENCY');

        $this->data["groups"]       = $this->users_model->fetch("groups");
        $this->data["ug"]           = $this->users_model->fetch("users_groups");

        //ITERATE ON THE USERS_GROUP TABLE
        foreach ($this->data["ug"] as $group) :
            if ($group->user_id == $user->id) :
                
                //GET THE GROUP ID OF THE CURRENT USER
                $this->data['g_id']   = $group->group_id;
                
                //GET THE GROUP NAME OF THE CURRENT USER
                if (is_array($this->data["groups"])) :
                    foreach ($this->data["groups"] as $name) :
                        if ($name->id == $group->group_id) :
                            $this->data['group_name'] = $name->name;
                        endif;
                    endforeach;                
                endif;
            endif;
        endforeach;

        $this->template->content->view('admin/users/edit', $this->data);
        
        // publish the template
        $this->template->publish();
    }

    //DELETE A USER
    public function delete()
    {
        // do we have the right userlevel?
        if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) :

            $this->ion_auth->delete_user($this->uri->segment(3));        
            //redirect them back to the auth page
            redirect('users/listing', 'refresh');
        else:

            $this->session->set_flashdata('message', $this->ion_auth->messages());
            //redirect them back to the auth page
            redirect('users/listing', 'refresh');
        endif;
    }    

    public function group()
    {
        $this->template->title                  = 'Users Group';

        if (!$this->ion_auth->logged_in()) :
        
            //redirect them to the login page
            redirect('auth/login', 'refresh');
        elseif (!$this->ion_auth->is_admin()) :
        
            //redirect them to the home page because they must be an administrator to view this
            redirect('users/index', 'refresh');
        else :
            
            $this->load->library('pagination');

            $config                 = array();
            $config["base_url"]     = base_url('users/group');
            $config["total_rows"]   = $this->users_model->get_record_count('groups');
            $config["per_page"]     = 10;
            $config["uri_segment"]  = 3;

            //ADDITIONAL PAGINATION STYLING
            $config['full_tag_open']    = '<div class="pagination"><ul>';
            $config['full_tag_close']   = '</ul></div>';
            $config['cur_tag_open']     = '<li class="active"><a href="javascript:void(0)">';
            $config['cur_tag_close']    = '</a></li>';
            $config['num_tag_open']     = '<li>';
            $config['num_tag_close']    = '</li>';
            $config['last_tag_open']    = '<li>';
            $config['last_tag_close']   = '</li>';
            $config['next_tag_open']    = '<li>';
            $config['next_tag_close']   = '</li>';
            $config['prev_tag_open']    = '<li>';
            $config['prev_tag_close']   = '<li>';

            $this->pagination->initialize($config);

            $page               = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["groups"]     = $this->users_model->fetch("groups", $config["per_page"], $page);
            $data["links"]      = $this->pagination->create_links();

            $this->template->content->view('admin/users/group/list', $data);
            
            // publish the template
            $this->template->publish();
        endif;
    }

    public function create_group()
    {
        $this->template->title                  = 'Create Group';

        if (!$this->ion_auth->logged_in()) :
            //redirect them to the login page
            redirect('auth/login', 'refresh');
        elseif (!$this->ion_auth->is_admin()) :
            //redirect them to the home page because they must be an administrator to view this
            redirect('users/index', 'refresh');
        else :

            if (isset($_POST['create_group'])) :

                $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');

                if ($this->form_validation->run() == false) :
                    $data['error'] = $this->session->set_flashdata('error', 'Sorry, there is problem. Please try again.');
                else:

                    $records = array(
                        'name'          => $this->input->post('name'),
                        'description'   => $this->input->post('description')
                    );

                    if ( $this->users_model->insert_record("groups", $records) ):
                        $data['error']      = $this->session->set_flashdata('error', 'We have problem inserting the group record.');
                        redirect('users/group', 'refresh', $data);
                    else:
                        $data['success']    = $this->session->set_flashdata('success', 'You have creted a user group.');
                        redirect('users/group', 'refresh', $data);
                    endif;
                endif;
            endif;

            $this->template->content->view('admin/users/group/create', $data);
            
            // publish the template
            $this->template->publish();
        endif;
    }

    public function edit_group()
    {
        $this->template->title                  = 'Edit Group';

        if (!$this->ion_auth->logged_in()) :
            //redirect them to the login page
            redirect('auth/login', 'refresh');
        elseif (!$this->ion_auth->is_admin()) :
            //redirect them to the home page because they must be an administrator to view this
            redirect('users/index', 'refresh');
        else :

            if (isset($_POST['edit_group'])) :
            
                $id = $this->input->post('id_group');

                if ($this->form_validation->run() == false) :
                    $data['error'] = $this->session->set_flashdata('error', "Fields with * are required.");
                    redirect("users/edit_group/$id", 'refresh');
                else:
                    $data['success'] = $this->session->set_flashdata('success', "Edited successfully");
                    redirect("users/edit_group/$id", 'refresh');
                endif;
            endif;
            
            $data['group'] = $this->users_model->fetch_where('groups', 'id', $this->uri->segment(3));
            $this->template->content->view('admin/users/group/edit', $data);
            
            // publish the template
            $this->template->publish();
        endif;
    }

    public function delete_group()
    {
        // do we have the right userlevel?
        if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) :

            $data = array('id' => $this->uri->segment(3) );
            $this->users_model->delete_record('groups', $data);
            
            $data['success'] = $this->session->set_flashdata('success', "Deleted successfully");
            redirect("users/group", 'refresh');
        else:

            $this->session->set_flashdata('error', "Sorry, you do not have access right.");
            //redirect them back to the auth page
            redirect('users/listing', 'refresh');
        endif;
    }

    public function history()
    {
        if (!$this->ion_auth->logged_in()) :
            //redirect them to the login page
            redirect('auth/login', 'refresh');
        elseif (!$this->ion_auth->is_admin()) :
            //redirect them to the home page because they must be an administrator to view this
            redirect('users/index', 'refresh');        
        else :

            $this->template->title = 'Leave History';

            $this->data['user_history'] = $this->users_model->fetch_where_array('leaves', 'id' , $this->uri->segment(3));
            $this->data['user']         = $this->users_model->fetch_where('users', "id" , $this->uri->segment(3));
            $this->template->content->view('admin/users/history', $this->data);
            
            // publish the template
            $this->template->publish();
        endif;
    }

    public function check_default($post_string)
    {
      return $post_string == '0' ? FALSE : TRUE;
    }
    
    //EDIT MY PROFILE
    public function edit_my_profile()
    {
        $user = $this->ion_auth->user($id)->row();

        $this->data['title']    = "Edit My Profile";        

        $this->template->javascript->add('assets/js/myscript.js');         

        //validate form input
        $this->form_validation->set_rules('first_name', 'First Name', 'required|xss_clean');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required|xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('position','Position','required|callback_check_default');

        $this->form_validation->set_message('check_default', 'You must select a company position for the user.');

        if (isset($_POST['save_my_profile']) && !empty($_POST['save_my_profile']))
        {           

            $data = array(
                'prefix'            => $this->input->post('prefix'),
                'first_name'        => $this->input->post('first_name'),
                'middle_name'       => $this->input->post('middle_name'),
                'last_name'         => $this->input->post('last_name'),
                'nationality'       => $this->input->post('nationality'),
                'position'          => $this->input->post('position'),
                'job_scope'         => $this->input->post('job_scope'),
                'email'             => $this->input->post('email'),
                'street_addr'       => $this->input->post('street_addr'),
                'town_city_addr'    => $this->input->post('town_city_addr'),
                'post_code_addr'    => $this->input->post('post_code_addr'),
                'country_addr'      => $this->input->post('country_addr'),
                'phone'             => $this->input->post('phone'),
                'm_phone'           => $this->input->post('m_phone'),
                'bank_name'         => $this->input->post('bank_name'),
                'acct_name'         => $this->input->post('acct_name'),
                'acct_number'       => $this->input->post('acct_number'),
                'others'            => $this->input->post('others')         
            );

            //update the password if it was posted
            if ($this->input->post('password')) :
            
                $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
                $this->form_validation->set_rules('password_confirm', 'Password Confirmation', 'required');

                $data['password'] = $this->input->post('password');
            
            endif;

            if ( $this->form_validation->run() ) :
            
                $this->ion_auth->update($this->input->post('id'), $data);

                //check to see if we are creating the user
                //redirect them back to the admin page
                $this->session->set_flashdata('message', "Edited successfully");
                redirect("users/edit_my_profile/$id", 'refresh');
            
            endif;

        }
        
        //display the edit user form
        $this->data['csrf'] = $this->_get_csrf_nonce();

        //set the flash data error message if there is one
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

        //pass the user to the view
        $this->data['user']             = $user;

        $this->data['prefix'] = array(
            'name'  => 'prefix',
            'id'    => 'prefix',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('prefix', $user->prefix)
        );
        $this->data['first_name'] = array(
            'name'  => 'first_name',
            'id'    => 'first_name',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('first_name', $user->first_name)
        );
        $this->data['middle_name'] = array(
            'name'  => 'middle_name',
            'id'    => 'middle_name',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('middle_name', $user->middle_name)
        );      
        $this->data['last_name'] = array(
            'name'  => 'last_name',
            'id'    => 'last_name',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('last_name', $user->last_name)
        );
        $this->data['nationality'] = array(
            'name'  => 'nationality',
            'id'    => 'nationality',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('nationality', $user->nationality)
        );      
        $this->data['street_addr'] = array(
            'name'  => 'street_addr',
            'id'    => 'street_addr',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('street_addr', $user->street_addr)
        );
        $this->data['town_city_addr'] = array(
            'name'  => 'town_city_addr',
            'id'    => 'town_city_addr',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('town_city_addr', $user->town_city_addr)
        );
        $this->data['post_code_addr'] = array(
            'name'  => 'post_code_addr',
            'id'    => 'post_code_addr',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('post_code_addr', $user->post_code_addr)
        );
        $this->data['country_addr'] = array(
            'name'  => 'country_addr',
            'id'    => 'country_addr',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('country_addr', $user->country_addr)
        );
        $this->data['email'] = array(
            'name'  => 'email',
            'id'    => 'email',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('email', $user->email, $user->email)
        );      
        $this->data['position'] = array(
            'name'  => 'position',
            'id'    => 'position',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('position', $user->position)
        );
        $this->data['job_scope'] = array(
            'name'  => 'job_scope',
            'id'    => 'job_scope',
            'value' => $this->form_validation->set_value('job_scope', $user->job_scope)
        );      
        $this->data['phone'] = array(
            'name'  => 'phone',
            'id'    => 'phone',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('phone', $user->phone)
        );
        $this->data['m_phone'] = array(
            'name'  => 'm_phone',
            'id'    => 'm_phone',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('m_phone', $user->m_phone)
        );
        $this->data['annual_leaves'] = array(
            'name'  => 'annual_leaves',
            'id'    => 'annual_leaves',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('annual_leaves', $user->annual_leaves)
        );
        $this->data['sick_leaves'] = array(
            'name'  => 'sick_leaves',
            'id'    => 'sick_leaves',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('sick_leaves', $user->sick_leaves)
        );
        $this->data['maternity_leaves'] = array(
            'name'  => 'maternity_leaves',
            'id'    => 'maternity_leaves',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('maternity_leaves', $user->maternity_leaves)
        );              
        $this->data['employment_date'] = array(
            'name'  => 'employment_date',
            'id'    => 'edit_employment_date',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('employment_date', date("F d, Y", strtotime($user->employment_date)))
        );

        $this->data['monthly_salary'] = array(
            'name'  => 'monthly_salary',
            'id'    => 'monthly_salary',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('monthly_salary', number_format($user->monthly_salary, 2, '.', ''))
        );

        //BANK DETAILS
        $this->data['bank_name'] = array(
            'name'  => 'bank_name',
            'id'    => 'bank_name',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('bank_name', $user->bank_name)
        );
        $this->data['acct_name'] = array(
            'name'  => 'acct_name',
            'id'    => 'acct_name',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('acct_name', $user->acct_name)
        );
        $this->data['acct_number'] = array(
            'name'  => 'acct_number',
            'id'    => 'acct_number',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('acct_number', $user->acct_number)
        );      
        $this->data['others'] = array(
            'name'  => 'others',
            'id'    => 'others',
            'value' => $this->form_validation->set_value('others', $user->others)
        );
        //SAVING A USER     
        $this->data['password'] = array(
            'name' => 'password',
            'id'   => 'password',
            'type' => 'password'
        );
        $this->data['password_confirm'] = array(
            'name' => 'password_confirm',
            'id'   => 'password_confirm',
            'type' => 'password'
        );

        $this->data['hired_since'] = array(
            'name' => 'hired_since',
            'id'   => 'hired_since',
            'type' => 'text',
            'value' => $this->form_validation->set_value('hired_since', $this->tools_model->get_hiring_date($user->employment_date))
        );

        $this->data['default_currency'] = $this->tools_model->get_settings('CURRENCY');

        $this->template->content->view('admin/users/update_profile', $this->data);
        
        // publish the template
        $this->template->publish();
    }    
}