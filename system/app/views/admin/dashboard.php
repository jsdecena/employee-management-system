<?php if ( isset($user)) :  ?>
	<div id="main_content" class="span9">

		<div class="bs-docs-example calendar">				
			
			<div class="controls">
			
				<blockquote>
				  <p>Hello, <?php echo $user->first_name . ' ' . $user->middle_name . ' ' . $user->last_name; ?></p>
				  <small><?php echo $user->position; ?></small>
				  <small>Hired since: <strong><?php echo $hired_since; ?></strong></small>
				</blockquote>

				<div class="control-group">
					<div class="controls">
						<h5>Annual leaves left: <?php echo $user->annual_leaves; ?></h5>
						<div class="progress progress-info progress-striped">
						  <div class="bar" style="width: <?php echo $annual_leaves_left; ?>%"></div>
						</div>						
					</div>

					<div class="controls">
						<h5>Sick leaves left: <?php echo $user->sick_leaves; ?> </h5>
						<div class="progress progress-success progress-striped">
						  <div class="bar" style="width: <?php echo $sick_leaves_left; ?>%"></div>
						</div>
					</div>
					
					<?php if ( $user->prefix == 'Ms' || $user->prefix == 'Mrs') : ?>
						<div class="controls">						
							<h5>Maternity leaves left: <?php echo $user->maternity_leaves; ?> </h5>
							<div class="progress progress-warning progress-striped">
							  <div class="bar" style="width: <?php echo $maternity_leaves_left; ?>%"></div>
							</div>							
						</div>
					<?php endif; ?>
				</div>

			</div>


			<div class="control-group">

				<?php echo $this->load->view('admin/calendar'); ?>

				<div class="controls pull-left" style="display:none">
					<label for="date_from">From: </label>
					<div class="input-append">
					  <input class="span2" id="date_from" type="text">
					  <span class="add-on"><i class="icon-calendar">&nbsp;</i></span>
					</div>			
				</div>

				<div class="controls pull-left" style="display:none">
					<label for="date_to">To: </label>
					<div class="input-append">
					  <input class="span2" id="date_to" type="text">
					  <span class="add-on"><i class="icon-calendar">&nbsp;</i></span>
					</div>			
				</div>								

			</div>			
		</div>	

	</div>

<?php endif; ?>