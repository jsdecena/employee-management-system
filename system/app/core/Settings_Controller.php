<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_Controller extends MY_Controller {

	function __construct() {

		parent::__construct();
	}
	public function index(){

		$this->template->title = 'Settings';

		$this->load->model('settings_model');

		//GET THE CURRENCY SETTINGS
		$this->data['set_currency'] = $this->tools_model->get_settings('CURRENCY');

		$this->data['users_per_page'] = array(
			'name'  => 'users_per_page',
			'id'    => 'users_per_page',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('users_per_page', $this->tools_model->get_settings('USERS_PER_PAGE'))
		);

		$this->data['software_name'] = array(
			'name'  => 'software_name',
			'id'    => 'software_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('software_name', $this->tools_model->get_settings('SOFTWARE_NAME'))
		);

		$this->data['admin_name'] = array(
			'name'  => 'admin_name',
			'id'    => 'admin_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('admin_name', $this->tools_model->get_settings('ADMIN_NAME'))
		);

		$this->data['admin_email'] = array(
			'name'  => 'admin_email',
			'id'    => 'admin_email',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('admin_email', $this->tools_model->get_settings('ADMIN_EMAIL'))
		);		

		$this->data['annual_leaves'] = array(
			'name'  => 'annual_leaves',
			'id'    => 'annual_leaves',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('annual_leaves', $this->tools_model->get_settings('annual_leaves'))
		);

		$this->data['sick_leaves'] = array(
			'name'  => 'sick_leaves',
			'id'    => 'sick_leaves',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('sick_leaves', $this->tools_model->get_settings('sick_leaves'))
		);

		$this->data['maternity_leaves'] = array(
			'name'  => 'maternity_leaves',
			'id'    => 'maternity_leaves',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('maternity_leaves', $this->tools_model->get_settings('maternity_leaves'))
		);		

		if ( $this->input->post('submit')) :

			$this->session->set_flashdata('message', 'Settings updated!');
			$this->set_settings();

		endif;

		$this->data['s_name'] = $this->tools_model->get_settings('SOFTWARE_NAME');

		$this->template->content->view('admin/settings', $this->data);

		// publish the template
        $this->template->publish();

	}

	public function set_settings() {

		//SET THE CURRENCY SETTINGS
		$this->data['set_currency'] 				= $this->tools_model->set_currency('CURRENCY');
		$this->data['users_per_page'] 				= $this->tools_model->set_users_per_page('USERS_PER_PAGE');
		$this->data['software_name'] 				= $this->tools_model->set_software_name('SOFTWARE_NAME');
		$this->data['annual_leaves'] 				= $this->tools_model->set_annual_leaves('ANNUAL_LEAVES');
		$this->data['sick_leaves'] 					= $this->tools_model->set_sick_leaves('SICK_LEAVES');
		$this->data['maternity_leaves'] 			= $this->tools_model->set_maternity_leaves('MATERNITY_LEAVES');

		//redirect them back to the auth page
		redirect('settings', 'refresh');
	}	
}