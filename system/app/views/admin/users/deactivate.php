<div id="main_content" class="span9">
	<div class="bs-docs-example">
		<h1>Deactivate User</h1>
		<p>Are you sure you want to deactivate the user '<?php echo $user->username; ?>'</p>
			
		<?php echo form_open("auth/deactivate/".$user->id);?>

		<div class="control-group">
			<div class="controls">
			  	<label for="confirm" class="pull-left">Yes: &nbsp; </label>
			    <input type="radio" name="confirm" value="yes" checked="checked" class="pull-left" />			
			</div>

			<div class="controls">
			  	<label for="confirm" class="pull-left">No: &nbsp; </label>
			    <input type="radio" name="confirm" value="no" class="pull-left" />			
			</div>

			<div class="controls">
				<?php echo form_submit('submit', 'Deactivate', 'class="btn btn-warning"');?>			
			</div>		
		</div>

		  <?php echo form_hidden($csrf); ?>
		  <?php echo form_hidden(array('id'=>$user->id)); ?>

		<?php echo form_close();?>		
	</div>

</div>