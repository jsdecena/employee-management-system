<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Leave_Controller extends MY_Controller {

    function __construct() 
    {
        parent::__construct();

        $this->load->model('leaves_model');
        $this->load->model('tools_model');
        $this->load->library('email');
        $this->load->model('users_model');
    }

    //redirect if needed, otherwise display the user list
    public function index() 
    {
        if (!$this->ion_auth->logged_in()) :
            //redirect them to the login page
            redirect('auth/login', 'refresh');
        else :
            redirect('leave/apply', 'refresh');
        endif;
    }

    public function apply()
    {        
        if (!$this->ion_auth->logged_in()) :        
            //redirect them to the login page
            redirect('auth/login', 'refresh');        
        endif;

        $this->template->title = 'Apply for a leave';
        $this->template->javascript->add('assets/js/myscript.js');


        $data['user']                   = $this->ion_auth->user()->row();
        $data['hired_since']            = $this->tools_model->get_hiring_date($this->tools_model->fetch_hiring_date($data['user']->id));
        $data['s_name']                 = $this->tools_model->get_settings('SOFTWARE_NAME');

        //SET THE REQUIRED FIELDS
        $this->form_validation->set_rules('leave_type', 'Leave Type', 'trim|required|xss_clean|callback_leave_type_check');
        $this->form_validation->set_rules('end_date', 'End Date', 'trim|xss_clean|callback_check_end_date');
        $this->form_validation->set_rules('start_date', 'Start Date', 'trim|xss_clean');
        $this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');
        $this->form_validation->set_rules('shift', 'Shift', 'trim|required|xss_clean');

        $this->form_validation->set_rules('leave_timing', 'Leave Type', 'callback_leave_timing_check');
    
        //CUSTOM MESSAGE FOR THE ROLE TO BE CHOSEN FIRST.
        $this->form_validation->set_message('leave_type_check', 'You must select the type of leave you are applying.');
        $this->form_validation->set_message('leave_timing_check', 'You select the timing you are applying.');
        $this->form_validation->set_message('check_end_date', 'Your end date must not be lower than the start date.');
        
        if ($this->form_validation->run() == true) :

            //INSERT THE LEAVE APPLICATION REQUEST TO THE DATABASE            
            $this->leaves_model->application_insert();

            //FETCH THE CURRENTLY LOGGED IN USER
            $user = $this->users_model->fetch_where('users', 'email', $this->session->userdata('email'));

            $_POST['name'] = $user->first_name ." ". $user->last_name;

            //EMAIL THE ADMIN ABOUT THE REQUEST
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => 'tls://smtp.gmail.com',
                'smtp_port' => 465,
                'smtp_user' => 'marq.cruz99@gmail.com',
                'smtp_pass' => 'Simonsj0214',
                'mailtype'  => 'html',
                'charset'   => 'utf-8'
            );

            $this->email->set_newline("\r\n");
            $this->email->from($user->email);
            $this->email->to($this->tools_model->get_settings('ADMIN_EMAIL'), $this->tools_model->get_settings('ADMIN_NAME'));
            $this->email->subject('Leave Application');
            $body = $this->load->view('admin/email/leave/apply', $_POST, TRUE);
            $this->email->message($body);

            if (!$this->email->send()){
                $data['error'] = $this->session->set_flashdata('error', 'We have problem sending the email. Please try again.');
            }

            $data['success'] = $this->session->set_flashdata('success', 'You have successfully applied your leave!');
            redirect("leave/apply", 'refresh', $data);
        else :

            //DISPLAY THE LEAVE APPLICATION FORM
            //set the flash data error message if there is one
            $data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');            

            $data['leave_type'] = array(
                'Annual Leave'       => 'Annual Leave',
                'Sick Leave'         => 'Sick Leave',
                'Maternity Leave'    => 'Maternity Leave'
            );

            $data['leave_timing'] = array(
                'Full Day'         => 'Full Day',
                'Half Day'         => 'Half Day'
            );

            $data['shift'] = array(
                '0'                 => 'Select Shift',
                'morning'           => 'Morning',
                'mid'               => 'Mid Day',
                'night'             => 'Night'
            );                

            $data['title'] = array(
                'name'  => 'title',
                'id'    => 'title',
                'type'  => 'text',
                'class' => 'input-xxlarge',
                'value' => $this->form_validation->set_value('title', $this->input->post('title'))
            );

            $data['description'] = array(
                'name'  => 'description',
                'id'    => 'description',
                'class' => 'input-xxlarge',
                'value' => $this->form_validation->set_value('description', $this->input->post('description')),
                'rows'  => 10,
                'cols'  => 30
            );               
            
            $data['date_applied'] = array(
                'data-format' => 'yyyy-MM-dd',
                'name'  => 'date_applied',
                'id'    => 'date_applied',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('date_applied', $this->input->post('date_applied'))
            );

            $data['half_date'] = array(
                'data-format' => 'yyyy-MM-dd',
                'name'  => 'half_date',
                'id'    => 'half_date',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('half_date', $this->input->post('half_date'))
            );

            $data['start_date'] = array(
                'data-format' => 'yyyy-MM-dd hh:mm:ss',
                'name'  => 'start_date',
                'id'    => 'start_date',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('start_date', $this->input->post('start_date'))
            ); 

            $data['end_date'] = array(
                'data-format' => 'yyyy-MM-dd hh:mm:ss',
                'name'  => 'end_date',
                'id'    => 'end_date',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('end_date', $this->input->post('end_date'))
            );


            //HIDDEN FIELDS
            $data['id_user'] = array(
                'name'  => 'id_user',
                'id'    => 'id_user',
                'type'  => 'hidden',
                'class' => 'btn btn-primary',
                'value' => $this->the_user->id
            );

            $data['apply'] = array(
                'value'               => 'Apply',
                'class'               => 'btn btn-primary',
                'name'                => 'apply'
            );

            $this->template->content->view('admin/leave/apply', $data);

            // publish the template
            $this->template->publish();
        endif;
    }

    //DISPLAY PENDING APPLICATIONS
    public function pending()
    {
        if (!$this->ion_auth->logged_in()) :
        
            //redirect them to the login page
            redirect('auth/login', 'refresh');
        elseif (!$this->ion_auth->is_admin()) :
        
            //redirect them to the home page because they must be an administrator to view this
            redirect('users/index', 'refresh');
        else :

            $this->template->title = 'Leave Applications';

            $this->load->library('pagination');

            $config                 = array();
            $config["base_url"]     = base_url('leave/pending');
            $config["total_rows"]   = $this->leaves_model->status_count(0); // 0 = Pending
            $config["per_page"]     = 10;
            $config["uri_segment"]  = 3;

            //ADDITIONAL PAGINATION STYLING
            $config['full_tag_open']    = '<div class="pagination"><ul>';
            $config['full_tag_close']   = '</ul></div>';
            $config['cur_tag_open']     = '<li class="active"><a href="javascript:void(0)">';
            $config['cur_tag_close']    = '</a></li>';
            $config['num_tag_open']     = '<li>';
            $config['num_tag_close']    = '</li>';
            $config['last_tag_open']    = '<li>';
            $config['last_tag_close']   = '</li>';
            $config['next_tag_open']    = '<li>';
            $config['next_tag_close']   = '</li>';
            $config['prev_tag_open']    = '<li>';
            $config['prev_tag_close']   = '<li>';

            $this->pagination->initialize($config);

            $page                       = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $this->data["leaves"]       = $this->leaves_model->fetch_leave_applications($config["per_page"], $page, 0);
            $this->data["links"]        = $this->pagination->create_links();

            $this->template->content->view('admin/leave/pending', $this->data);
            
            // publish the template
            $this->template->publish();
        endif;
    }

    //DISPLAY APPROVED APPLICATIONS
    public function approved()
    {
        if (!$this->ion_auth->logged_in()) :
        
            //redirect them to the login page
            redirect('auth/login', 'refresh');
        elseif (!$this->ion_auth->is_admin()) :
        
            //redirect them to the home page because they must be an administrator to view this
            redirect('users/index', 'refresh');
        else :

            $this->template->title = 'Leave Applications';

            $this->load->library('pagination');

            $config                 = array();
            $config["base_url"]     = base_url('leave/leave_list');
            $config["total_rows"]   = $this->leaves_model->status_count(1); // 1 = approved
            $config["per_page"]     = 10;
            $config["uri_segment"]  = 3;

            //ADDITIONAL PAGINATION STYLING
            $config['full_tag_open']    = '<div class="pagination"><ul>';
            $config['full_tag_close']   = '</ul></div>';
            $config['cur_tag_open']     = '<li class="active"><a href="javascript:void(0)">';
            $config['cur_tag_close']    = '</a></li>';
            $config['num_tag_open']     = '<li>';
            $config['num_tag_close']    = '</li>';
            $config['last_tag_open']    = '<li>';
            $config['last_tag_close']   = '</li>';
            $config['next_tag_open']    = '<li>';
            $config['next_tag_close']   = '</li>';
            $config['prev_tag_open']    = '<li>';
            $config['prev_tag_close']   = '<li>';

            $this->pagination->initialize($config);

            $page                 = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["leaves"]       = $this->leaves_model->fetch_leave_applications($config["per_page"], $page, 1);
            $data["links"]        = $this->pagination->create_links();

            $this->template->content->view('admin/leave/approved', $data);
            
            // publish the template
            $this->template->publish();            
        endif;
    }

    //DISPLAY DENIED APPLICATIONS
    public function denied()
    {
        if (!$this->ion_auth->logged_in()) :
        
            //redirect them to the login page
            redirect('auth/login', 'refresh');
        elseif (!$this->ion_auth->is_admin()) :
        
            //redirect them to the home page because they must be an administrator to view this
            redirect('users/index', 'refresh');
        else :

            $this->template->title = 'Leave Applications';

            $this->load->library('pagination');

            $config                 = array();
            $config["base_url"]     = base_url('leave/denied');
            $config["total_rows"]   = $this->leaves_model->status_count(2); // 2 = denied
            $config["per_page"]     = 10;
            $config["uri_segment"]  = 3;

            //ADDITIONAL PAGINATION STYLING
            $config['full_tag_open']    = '<div class="pagination"><ul>';
            $config['full_tag_close']   = '</ul></div>';
            $config['cur_tag_open']     = '<li class="active"><a href="javascript:void(0)">';
            $config['cur_tag_close']    = '</a></li>';
            $config['num_tag_open']     = '<li>';
            $config['num_tag_close']    = '</li>';
            $config['last_tag_open']    = '<li>';
            $config['last_tag_close']   = '</li>';
            $config['next_tag_open']    = '<li>';
            $config['next_tag_close']   = '</li>';
            $config['prev_tag_open']    = '<li>';
            $config['prev_tag_close']   = '<li>';

            $this->pagination->initialize($config);

            $page                 = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["leaves"]       = $this->leaves_model->fetch_leave_applications($config["per_page"], $page, 2);
            $data["links"]        = $this->pagination->create_links();

            $this->template->content->view('admin/leave/denied', $data);
            
            // publish the template
            $this->template->publish();
        endif;
    }    

    //APPROVE AN APPLICATION
    public function approve($id)
    {
        if (!$this->ion_auth->logged_in()) :
            //redirect them to the login page
            redirect('auth/login', 'refresh');
        elseif (!$this->ion_auth->is_admin()) :
            //redirect them to the home page because they must be an administrator to view this
            redirect('users/index', 'refresh');
        else :

            $this->leaves_model->approve_application($this->uri->segment(3), $this->the_user->first_name . " "  . $this->the_user->last_name);
            $data['success'] = $this->session->set_flashdata('success', 'You have successfully approved this application!');
            redirect("leave/pending", 'refresh', $data);
        endif;
    }

    //DENY AN APPLICATION
    public function deny()
    {
        if (!$this->ion_auth->logged_in()) :        
            //redirect them to the login page
            redirect('auth/login', 'refresh');
        elseif (!$this->ion_auth->is_admin()) :
            //redirect them to the home page because they must be an administrator to view this
            redirect('users/index', 'refresh');
        else :

            $this->leaves_model->disapprove_application($this->uri->segment(3), $this->the_user->id);
            $data['success'] = $this->session->set_flashdata('success', 'You have successfully disapproved this application!');
            redirect("leave/approved", 'refresh', $data);
        endif;        
    }

    public function action()
    {
        if (isset($_POST['save'])) :

            $record = array(
                'status'  => $this->input->post('status'),
                'admin'   => $this->input->post('employee'),
                'reason'  => $this->input->post('reason')
            );

            $this->tools_model->update_record("leaves", "leave_id", $this->input->post('leave_id'), $record);

            $data['success'] = $this->session->set_flashdata('success', 'You have successfully approved this application!');
            redirect("leave/pending", 'refresh', $data);
        endif;

        $this->template->title = 'Application Actions';        

        $data['employee']    = $this->the_user->first_name[0] . ". " . $this->the_user->last_name;
        $data["leave"]       = $this->users_model->fetch_where("leaves", "leave_id", $this->uri->segment(3));
        $this->template->content->view('admin/leave/action', $data);

        // publish the template
        $this->template->publish();
    }

    //ADDITIONAL CHECKING FOR TYPE OF LEAVE
    public function leave_type_check($str) {
        if ($str == '0') { return FALSE; }
    }

    public function leave_timing_check($str) {
        if ($str == '0') { return FALSE; }
    }

    //CHECK FOR THE END LEAVE DATE
    //IT MUST NOT BE LOWER THAN THE START DATE
    public function check_end_date(){

        $date1 = strtotime($this->input->post('start_date'));
        $date2 = strtotime($this->input->post('end_date'));

        if ($date2 < $date1) { return FALSE;  }else{ return TRUE; }

    }
}