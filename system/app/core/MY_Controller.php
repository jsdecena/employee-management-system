<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
    
    protected $the_user;

    public function __construct(){

        parent::__construct();
        $this->load->library('template');
        $this->load->library('ion_auth');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->helper('url');

        $this->load->model('tools_model');   

        // Load MongoDB library instead of native db driver if required
        $this->config->item('use_mongodb', 'ion_auth') ?
        $this->load->library('mongo_db') :

        $this->load->database();

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        //===================== THIS IS FOR THE TEMPLATE LIBRARY =====================//
        
        // START DYNAMICALLY ADD STYLESHEETS
        $css = array(
            'assets/css/bootstrap.css',
            'assets/css/bootstrap-responsive.css',
            'assets/css/docs.css',
            'assets/css/admin.css',
            'assets/css/bootstrap-datetimepicker.min.css',
            'assets/js/google-code-prettify/prettify.css',

            // GOOGLE FONTS
            'http://fonts.googleapis.com/css?family=Coustard',
            'http://fonts.googleapis.com/css?family=Ubuntu',
            'http://fonts.googleapis.com/css?family=Open+Sans'
        );

        $this->template->stylesheet->add($css);
        // END DYNAMICALLY ADD STYLESHEETS

        // START DYNAMICALLY ADD JAVASCRIPTS
        $js = array(
            'http://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.3/jquery.min.js',
            'http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js',
            'assets/js/bootstrap-tab.js',
            'assets/js/bootstrap-datetimepicker.min.js',
            'assets/js/google-code-prettify/prettify.js'
        );

        $this->template->javascript->add($js);

        $this->the_user = $this->ion_auth->user($id)->row();

        $this->template->set_template('admin/dashboard_tpl');
    }

    public function _get_csrf_nonce() {
        $this->load->helper('string');
        $key   = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    public function _valid_csrf_nonce() {
        
        if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE && $this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }  
}