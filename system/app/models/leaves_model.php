<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Leaves_model extends CI_Model {

    public function __construct() 
    {
        parent::__construct();
    }

    public function leave_applications($limit, $offset)
    {
    	$this->db->get('leaves', $limit, $offset);
    }

    /*
    * @param
    * 0 = pending
    * 1 = approved
    * 2 = denied
    */
    public function status_count($param)
    {
        $this->db->select('count(*) AS total');
        $this->db->where('status', $param);
        $query = $this->db->get('leaves');
        
        $data = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $sum = $row['total'];
            }
            return $sum;
        }
    }    

    //INSERT THE LEAVE APPLICATION INFO IN THE DATABASE
    public function application_insert()
    {
        $shift = (bool)$this->input->post('shift');

        if ( $shift !== true ) :

            //@id = User ID
            $data = array(
               'id'             => $this->input->post('id_user'),
               'title'          => $this->input->post('title'),
               'leave_type'     => $this->input->post('leave_type'),
               'leave_timing'   => $this->input->post('leave_timing'),
               'shift'          => $this->input->post('shift'),
               'description'    => $this->input->post('description'),
               'date_applied'   => date('Y-m-d H:i:s'),
               'half_date'      => $this->input->post('half_date'),
               'start_date'     => $this->input->post('start_date'),               
               'end_date'       => $this->input->post('end_date'),
               'no_days'        => $this->getNumberOfDays(strtotime($this->input->post('end_date')), strtotime($this->input->post('start_date'))),
               'status'         => 0, //UNAPPROVED,
               'date_modified'  => date('Y-m-d H:i:s')
            );            
        else:

            //@id = User ID
            $data = array(
               'id'             => $this->input->post('id_user'),
               'title'          => $this->input->post('title'),
               'leave_type'     => $this->input->post('leave_type'),
               'leave_timing'   => $this->input->post('leave_timing'),
               'shift'          => $this->input->post('shift'),
               'description'    => $this->input->post('description'),
               'date_applied'   => date('Y-m-d H:i:s'),
               'half_date'      => $this->input->post('half_date'),
               'start_date'     => $this->input->post('start_date'),               
               'end_date'       => $this->input->post('end_date'),
               'no_days'        => 1,
               'status'         => 0, //UNAPPROVED
               'date_modified'  => date('Y-m-d H:i:s')
            );
        endif;

		$this->db->insert('leaves', $data);
    }

    public function fetch_leave_applications($limit, $start, $where = null) 
    {
        $this->db->select('users.first_name, users.last_name, users.email, leaves.*');
        $this->db->join('users', 'cc_users.id = cc_leaves.id');
        $this->db->where('status', $where);
        $this->db->limit($limit, $start);
        $query = $this->db->get("leaves");

        $data = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

   public function getNumberOfDays($endTimeStamp, $startTimeStamp)
   {
        //COMPUTE FOR FULL DAYS
        $timeDiff = abs($endTimeStamp - $startTimeStamp);

        $numberDays = $timeDiff/86400;  // 86400 seconds in one day

        // and you might want to convert to integer
        return intval($numberDays);
   }    

}