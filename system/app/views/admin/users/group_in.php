<div id="main_content" class="span9">
	<div class="bs-docs-example">
		<div class="controls">
			<h2>Employee Group List</h2>
			<div class="control-group">
				<p>These are the employee group list.</p>
			</div>
		</div>
		<?php echo form_open('users/edit_user_group_list'); ?>
			<section>
				<div class="control-group">
					<label for="user_name">Name</label>
					<div class="controls">
						<input type="text" value="<?php echo $user_data->first_name . " ". $user_data->last_name ?>" id="user_name" />
					</div>
				</div>
				<div class="control-group">
					<label for="groups">Groups</label>
					<div class="controls">
						<select name="group_id" id="groups">
							<?php foreach ( $user_groups as $groups ) : ?>
								<option value="<?php echo $groups['id']; ?>"><?php echo $groups['name']; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<input type="hidden" name="users_group_id" value="<?php echo $this->uri->segment(4) ?>">
				<button name="save_user_group" class="btn btn-primary">Save</button>
			</section>
		<?php echo form_close(); ?>			
	</div>	
</div>