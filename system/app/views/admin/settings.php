<div id="main_content" class="span9">
	
	<div class="bs-docs-example pull-left <?php echo $this->uri->segment(2) ?> span8">
		<section>
			<legend>Software Settings</legend>
			
			<?php if ( $this->session->flashdata('message') ) : ?>
			
				<div id="infoMessage" class="alert-block alert-success">
					<a class="close" data-dismiss="alert"> <i class="icon-remove">&nbsp;</i></a>
					<?php echo $this->session->flashdata('message'); ?>
				</div>

			<?php endif; ?>

			<?php echo form_open('settings'); ?>
				
					<div class="control-group">
						<label for="software_name"> Software Name: </label>				
						<div class="controls">
							<?php echo form_input($software_name, '', 'class="input-block-level" placeholder="Your software name"');?>
						</div>						
					</div>

					<div class="control-group">
						<label for="admin_name"> Default Admin Name: </label>				
						<div class="controls">
							<?php echo form_input($admin_name, '', 'class="input-block-level" placeholder="Admin Name"');?>
						</div>						
					</div>

					<div class="control-group">
						<label for="admin_email"> Default Admin Email: </label>				
						<div class="controls">
							<?php echo form_input($admin_email, '', 'class="input-block-level" placeholder="Admin Email"');?>
						</div>						
					</div>

					<div class="control-group">
						<label for="annual_leaves">Default Annual Leaves</label>
						<div class="controls">
							<?php echo form_input($annual_leaves, '', 'class="input-block-level" placeholder="14"');?>							
						</div>
					</div>

					<div class="control-group">
						<label for="sick_leaves">Default Sick Leaves</label>
						<div class="controls">
							<?php echo form_input($sick_leaves, '', 'class="input-block-level" placeholder="7"');?>							
						</div>						
					</div>

					<div class="control-group">
						<label for="maternity_leaves">Default Maternity Leaves</label>
						<div class="controls">
							<?php echo form_input($maternity_leaves, '', 'class="input-block-level" placeholder="30"');?>
						</div>						
					</div>							
							
					<div class="control-group">
						<label for="set_currency">Default Currency</label>
						<div class="controls">
							<select id="set_currency" name="set_currency">
								<option value="Php"<?php if ( isset($set_currency) && $set_currency == 'Php') : ?> selected="selected"<?php endif; ?>> Php </option>
								<option value="SGD"<?php if ( isset($set_currency) && $set_currency == 'SGD') : ?> selected="selected"<?php endif; ?>> SGD </option>
							</select>
						</div>
					</div>

					<div class="control-group">
						<label for="users_per_page">Users per page in user's list</label>
						<div class="controls">
							<?php echo form_input($users_per_page, '', 'class="input-block-level" placeholder="10"');?>
						</div>						
					</div>			

					<div class="btn-group">
						<?php echo form_submit('submit', 'Save', 'class="btn btn-primary"'); ?>
					</div>
			<?php echo form_close(); ?>
			
			<div class="clearfix"></div>
		</section>
	</div>
</div>