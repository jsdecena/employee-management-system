<?php echo form_open("auth/login", "class=\"form-signin\" ");?>

  <?php if ( $this->session->flashdata('message') ) : ?>
    <div class="control-group">
      <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <?php echo $this->session->flashdata('message') ?>
      </div>
    </div>
  <?php endif; ?>

  <fieldset>    
    <legend class="form-signin-heading lead">Employee Management</legend>
    <?php echo form_input($identity, $this->input->post('identity'), 'class="input-block-level" placeholder="Email Address"');?>
    <input name="password" type="password" class="input-block-level" placeholder="Password">
    <button class="btn btn-primary" type="submit">Log in</button>
  </fieldset>
<?php echo form_close(); ?>