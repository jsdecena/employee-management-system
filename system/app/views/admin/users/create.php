<div id="main_content" class="span9">

	<div class="bs-docs-example pull-left">
		<div class="control-group">
			<div class="controls">
				<h2>Create an employee</h2>	

				<?php if ( $this->session->flashdata('success') ) : ?>
					<div class="alert-block alert-success">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<?php echo $this->session->flashdata('success') ?>
					</div>			
				<?php elseif ( $this->session->flashdata('error') ) : ?>
					<div class="alert-block alert-error">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php endif; ?>

				<?php echo form_open("users/create");?>

				<div class="control-group span8">

				<div class="personal_info">

					<ul class="nav nav-tabs" id="myTab">
					  <li class="active"><a href="#info">Basic Info.</a></li>
					  <li><a href="#extended">Extended Info.</a></li>
					</ul>
					 
					<div class="tab-content">

					  <div class="tab-pane active" id="info">

							<div class="controls well">

								<div class="btn-group" data-toggle="buttons-radio">
									<div class="btn">
										<span>Mr.</span> 
										<?php if ( $this->input->post('prefix') == 'Mr') : ?>
											<?php echo form_radio('prefix', 'Mr', TRUE ); ?>
										<?php else: ?>
											<?php echo form_radio('prefix', 'Mr' ); ?>
										<?php endif; ?>
									</div>
									<div class="btn">
										<span>Ms.</span>
										<?php if ( $this->input->post('prefix') == 'Ms') : ?>
											<?php echo form_radio('prefix', 'Ms', TRUE ); ?>
										<?php else: ?>
											<?php echo form_radio('prefix', 'Ms' ); ?>
										<?php endif; ?>										
									</div>
									<div class="btn">
										<span>Mrs.</span>
										<?php if ( $this->input->post('prefix') == 'Mrs') : ?>
											<?php echo form_radio('prefix', 'Mrs', TRUE ); ?>
										<?php else: ?>
											<?php echo form_radio('prefix', 'Mrs' ); ?>
										<?php endif; ?>										
									</div>
								</div>

								<label for="first_name"> First Name: </label>
								<?php echo form_input($first_name, '', 'class="input-xxlarge" placeholder="First Name"');?>

								<label for="middle_name"> Middle Name: </label>
								<?php echo form_input($middle_name, '', 'class="input-xxlarge" placeholder="Middle Name"');?>

								<label for="last_name"> Last Name: </label>
								<?php echo form_input($last_name, '', 'class="input-xxlarge" placeholder="Last Name"');?>

								<label for="nationality"> Nationality: </label>
								<?php echo form_input($nationality, '', 'class="input-xxlarge" placeholder="Nationality/Citizenship"');?>						

								<label for="position"> Company Position: </label>
								<select name="position" id="position">
									<option value="0">-- Select Position -- </option>
									<option value="Company Owner">Company Owner</option>
									<option value="Company Co-Owner">Company Co-Owner</option>
									<option value="Web Designer">Web Designer</option>
									<option value="Web Developer">Web Developer</option>
									<option value="Sales Consultant">Sales Consultant</option>
									<option value="Admin Assistant">HR / Admin</option>
								</select>				

								<label for="employee_type"> Employee Type: </label>
								<select name="employee_type" id="employee_type">
									<option value="0">-- Select Type -- </option>
									<option value="Full-time Employee" <?php if ( $this->input->post('employee_type') == 'Full-time Employee') : ?>selected="selected"<?php endif; ?>>Full-time Employee</option>
									<option value="Part-time Employee" <?php if ( $this->input->post('employee_type') == 'Part-time Employee') : ?>selected="selected"<?php endif; ?>>Part-time Employee</option>									
								</select>

								<label for="job_scope"> Job Scope: </label>
									<?php echo form_textarea($job_scope, '', 'class="input-xxlarge" placeholder="Job Scope"');?>

								<label for="email"> Email: <sup class="text-error">*</sup></label>
								<?php echo form_input($email, '', 'class="input-xxlarge" placeholder="Email"');?>
							</div>

							<div class="controls well">
								<label for="address"> Address: </label>

								<div class="control-group">
									<div class="controls">
										<label for="street_addr">Street Address</label>
										<?php echo form_input($street_addr, '', 'class="input-xxlarge" placeholder="Street"');?>
									</div>
									<div class="controls">
										<label for="town_city_addr">Town / City </label>
										<?php echo form_input($town_city_addr, '', 'class="input-xxlarge" placeholder="Town or City"');?>
									</div>
									<div class="controls">
										<label for="post_code_addr">Post Code </label>
										<?php echo form_input($post_code_addr, '', 'class="input-xxlarge" placeholder="Postal Code"');?>
									</div>
									<div class="controls">
										<label for="country_addr">Country </label>
										<select name="country_addr"> 
											<option value="" selected="selected">Select Country</option> 
											<option value="United States">United States</option> 
											<option value="United Kingdom">United Kingdom</option> 
											<option value="Afghanistan">Afghanistan</option> 
											<option value="Albania">Albania</option> 
											<option value="Algeria">Algeria</option> 
											<option value="American Samoa">American Samoa</option> 
											<option value="Andorra">Andorra</option> 
											<option value="Angola">Angola</option> 
											<option value="Anguilla">Anguilla</option> 
											<option value="Antarctica">Antarctica</option> 
											<option value="Antigua and Barbuda">Antigua and Barbuda</option> 
											<option value="Argentina">Argentina</option> 
											<option value="Armenia">Armenia</option> 
											<option value="Aruba">Aruba</option> 
											<option value="Australia">Australia</option> 
											<option value="Austria">Austria</option> 
											<option value="Azerbaijan">Azerbaijan</option> 
											<option value="Bahamas">Bahamas</option> 
											<option value="Bahrain">Bahrain</option> 
											<option value="Bangladesh">Bangladesh</option> 
											<option value="Barbados">Barbados</option> 
											<option value="Belarus">Belarus</option> 
											<option value="Belgium">Belgium</option> 
											<option value="Belize">Belize</option> 
											<option value="Benin">Benin</option> 
											<option value="Bermuda">Bermuda</option> 
											<option value="Bhutan">Bhutan</option> 
											<option value="Bolivia">Bolivia</option> 
											<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option> 
											<option value="Botswana">Botswana</option> 
											<option value="Bouvet Island">Bouvet Island</option> 
											<option value="Brazil">Brazil</option> 
											<option value="British Indian Ocean Territory">British Indian Ocean Territory</option> 
											<option value="Brunei Darussalam">Brunei Darussalam</option> 
											<option value="Bulgaria">Bulgaria</option> 
											<option value="Burkina Faso">Burkina Faso</option> 
											<option value="Burundi">Burundi</option> 
											<option value="Cambodia">Cambodia</option> 
											<option value="Cameroon">Cameroon</option> 
											<option value="Canada">Canada</option> 
											<option value="Cape Verde">Cape Verde</option> 
											<option value="Cayman Islands">Cayman Islands</option> 
											<option value="Central African Republic">Central African Republic</option> 
											<option value="Chad">Chad</option> 
											<option value="Chile">Chile</option> 
											<option value="China">China</option> 
											<option value="Christmas Island">Christmas Island</option> 
											<option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option> 
											<option value="Colombia">Colombia</option> 
											<option value="Comoros">Comoros</option> 
											<option value="Congo">Congo</option> 
											<option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option> 
											<option value="Cook Islands">Cook Islands</option> 
											<option value="Costa Rica">Costa Rica</option> 
											<option value="Cote D'ivoire">Cote D'ivoire</option> 
											<option value="Croatia">Croatia</option> 
											<option value="Cuba">Cuba</option> 
											<option value="Cyprus">Cyprus</option> 
											<option value="Czech Republic">Czech Republic</option> 
											<option value="Denmark">Denmark</option> 
											<option value="Djibouti">Djibouti</option> 
											<option value="Dominica">Dominica</option> 
											<option value="Dominican Republic">Dominican Republic</option> 
											<option value="Ecuador">Ecuador</option> 
											<option value="Egypt">Egypt</option> 
											<option value="El Salvador">El Salvador</option> 
											<option value="Equatorial Guinea">Equatorial Guinea</option> 
											<option value="Eritrea">Eritrea</option> 
											<option value="Estonia">Estonia</option> 
											<option value="Ethiopia">Ethiopia</option> 
											<option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option> 
											<option value="Faroe Islands">Faroe Islands</option> 
											<option value="Fiji">Fiji</option> 
											<option value="Finland">Finland</option> 
											<option value="France">France</option> 
											<option value="French Guiana">French Guiana</option> 
											<option value="French Polynesia">French Polynesia</option> 
											<option value="French Southern Territories">French Southern Territories</option> 
											<option value="Gabon">Gabon</option> 
											<option value="Gambia">Gambia</option> 
											<option value="Georgia">Georgia</option> 
											<option value="Germany">Germany</option> 
											<option value="Ghana">Ghana</option> 
											<option value="Gibraltar">Gibraltar</option> 
											<option value="Greece">Greece</option> 
											<option value="Greenland">Greenland</option> 
											<option value="Grenada">Grenada</option> 
											<option value="Guadeloupe">Guadeloupe</option> 
											<option value="Guam">Guam</option> 
											<option value="Guatemala">Guatemala</option> 
											<option value="Guinea">Guinea</option> 
											<option value="Guinea-bissau">Guinea-bissau</option> 
											<option value="Guyana">Guyana</option> 
											<option value="Haiti">Haiti</option> 
											<option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option> 
											<option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option> 
											<option value="Honduras">Honduras</option> 
											<option value="Hong Kong">Hong Kong</option> 
											<option value="Hungary">Hungary</option> 
											<option value="Iceland">Iceland</option> 
											<option value="India">India</option> 
											<option value="Indonesia">Indonesia</option> 
											<option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option> 
											<option value="Iraq">Iraq</option> 
											<option value="Ireland">Ireland</option> 
											<option value="Israel">Israel</option> 
											<option value="Italy">Italy</option> 
											<option value="Jamaica">Jamaica</option> 
											<option value="Japan">Japan</option> 
											<option value="Jordan">Jordan</option> 
											<option value="Kazakhstan">Kazakhstan</option> 
											<option value="Kenya">Kenya</option> 
											<option value="Kiribati">Kiribati</option> 
											<option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option> 
											<option value="Korea, Republic of">Korea, Republic of</option> 
											<option value="Kuwait">Kuwait</option> 
											<option value="Kyrgyzstan">Kyrgyzstan</option> 
											<option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option> 
											<option value="Latvia">Latvia</option> 
											<option value="Lebanon">Lebanon</option> 
											<option value="Lesotho">Lesotho</option> 
											<option value="Liberia">Liberia</option> 
											<option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option> 
											<option value="Liechtenstein">Liechtenstein</option> 
											<option value="Lithuania">Lithuania</option> 
											<option value="Luxembourg">Luxembourg</option> 
											<option value="Macao">Macao</option> 
											<option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option> 
											<option value="Madagascar">Madagascar</option> 
											<option value="Malawi">Malawi</option> 
											<option value="Malaysia">Malaysia</option> 
											<option value="Maldives">Maldives</option> 
											<option value="Mali">Mali</option> 
											<option value="Malta">Malta</option> 
											<option value="Marshall Islands">Marshall Islands</option> 
											<option value="Martinique">Martinique</option> 
											<option value="Mauritania">Mauritania</option> 
											<option value="Mauritius">Mauritius</option> 
											<option value="Mayotte">Mayotte</option> 
											<option value="Mexico">Mexico</option> 
											<option value="Micronesia, Federated States of">Micronesia, Federated States of</option> 
											<option value="Moldova, Republic of">Moldova, Republic of</option> 
											<option value="Monaco">Monaco</option> 
											<option value="Mongolia">Mongolia</option> 
											<option value="Montserrat">Montserrat</option> 
											<option value="Morocco">Morocco</option> 
											<option value="Mozambique">Mozambique</option> 
											<option value="Myanmar">Myanmar</option> 
											<option value="Namibia">Namibia</option> 
											<option value="Nauru">Nauru</option> 
											<option value="Nepal">Nepal</option> 
											<option value="Netherlands">Netherlands</option> 
											<option value="Netherlands Antilles">Netherlands Antilles</option> 
											<option value="New Caledonia">New Caledonia</option> 
											<option value="New Zealand">New Zealand</option> 
											<option value="Nicaragua">Nicaragua</option> 
											<option value="Niger">Niger</option> 
											<option value="Nigeria">Nigeria</option> 
											<option value="Niue">Niue</option> 
											<option value="Norfolk Island">Norfolk Island</option> 
											<option value="Northern Mariana Islands">Northern Mariana Islands</option> 
											<option value="Norway">Norway</option> 
											<option value="Oman">Oman</option> 
											<option value="Pakistan">Pakistan</option> 
											<option value="Palau">Palau</option> 
											<option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option> 
											<option value="Panama">Panama</option> 
											<option value="Papua New Guinea">Papua New Guinea</option> 
											<option value="Paraguay">Paraguay</option> 
											<option value="Peru">Peru</option> 
											<option value="Philippines">Philippines</option> 
											<option value="Pitcairn">Pitcairn</option> 
											<option value="Poland">Poland</option> 
											<option value="Portugal">Portugal</option> 
											<option value="Puerto Rico">Puerto Rico</option> 
											<option value="Qatar">Qatar</option> 
											<option value="Reunion">Reunion</option> 
											<option value="Romania">Romania</option> 
											<option value="Russian Federation">Russian Federation</option> 
											<option value="Rwanda">Rwanda</option> 
											<option value="Saint Helena">Saint Helena</option> 
											<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option> 
											<option value="Saint Lucia">Saint Lucia</option> 
											<option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option> 
											<option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option> 
											<option value="Samoa">Samoa</option> 
											<option value="San Marino">San Marino</option> 
											<option value="Sao Tome and Principe">Sao Tome and Principe</option> 
											<option value="Saudi Arabia">Saudi Arabia</option> 
											<option value="Senegal">Senegal</option> 
											<option value="Serbia and Montenegro">Serbia and Montenegro</option> 
											<option value="Seychelles">Seychelles</option> 
											<option value="Sierra Leone">Sierra Leone</option> 
											<option value="Singapore">Singapore</option> 
											<option value="Slovakia">Slovakia</option> 
											<option value="Slovenia">Slovenia</option> 
											<option value="Solomon Islands">Solomon Islands</option> 
											<option value="Somalia">Somalia</option> 
											<option value="South Africa">South Africa</option> 
											<option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option> 
											<option value="Spain">Spain</option> 
											<option value="Sri Lanka">Sri Lanka</option> 
											<option value="Sudan">Sudan</option> 
											<option value="Suriname">Suriname</option> 
											<option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option> 
											<option value="Swaziland">Swaziland</option> 
											<option value="Sweden">Sweden</option> 
											<option value="Switzerland">Switzerland</option> 
											<option value="Syrian Arab Republic">Syrian Arab Republic</option> 
											<option value="Taiwan, Province of China">Taiwan, Province of China</option> 
											<option value="Tajikistan">Tajikistan</option> 
											<option value="Tanzania, United Republic of">Tanzania, United Republic of</option> 
											<option value="Thailand">Thailand</option> 
											<option value="Timor-leste">Timor-leste</option> 
											<option value="Togo">Togo</option> 
											<option value="Tokelau">Tokelau</option> 
											<option value="Tonga">Tonga</option> 
											<option value="Trinidad and Tobago">Trinidad and Tobago</option> 
											<option value="Tunisia">Tunisia</option> 
											<option value="Turkey">Turkey</option> 
											<option value="Turkmenistan">Turkmenistan</option> 
											<option value="Turks and Caicos Islands">Turks and Caicos Islands</option> 
											<option value="Tuvalu">Tuvalu</option> 
											<option value="Uganda">Uganda</option> 
											<option value="Ukraine">Ukraine</option> 
											<option value="United Arab Emirates">United Arab Emirates</option> 
											<option value="United Kingdom">United Kingdom</option> 
											<option value="United States">United States</option> 
											<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option> 
											<option value="Uruguay">Uruguay</option> 
											<option value="Uzbekistan">Uzbekistan</option> 
											<option value="Vanuatu">Vanuatu</option> 
											<option value="Venezuela">Venezuela</option> 
											<option value="Viet Nam">Viet Nam</option> 
											<option value="Virgin Islands, British">Virgin Islands, British</option> 
											<option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option> 
											<option value="Wallis and Futuna">Wallis and Futuna</option> 
											<option value="Western Sahara">Western Sahara</option> 
											<option value="Yemen">Yemen</option> 
											<option value="Zambia">Zambia</option> 
											<option value="Zimbabwe">Zimbabwe</option>
										</select>								
									</div>
								</div>						
							</div>					

							<div class="controls well">
								<label for="phone"> Phone: </label>
								<?php echo form_input($phone, '', 'class="input-xxlarge" placeholder="Phone"');?>

								<label for="phone"> Mobile Phone: </label>
								<?php echo form_input($m_phone, '', 'class="input-xxlarge" placeholder="Mobile Phone"');?>
							</div>
					  	
					  </div>

					  <div class="tab-pane" id="extended">
					  	
							<div class="controls well">

								<label for="employment_date">Hired Date: </label>
								<div class="input-prepend controls datepicker">
									<span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
									<?php echo form_input($employment_date, '', 'class="input-xxlarge datepicker"');?>									
								</div>	

								<label for="monthly_salary"> Monthly Salary: </label>
								<div class="input-prepend controls">							
									<span class="add-on">Php</span>
									<?php echo form_input($monthly_salary, '', 'class="input-xxlarge" placeholder="30,000"');?>
								</div>

							</div>

							<div class="controls well">
								<div class="control-group">
										
										<label> Bank Details</label>

									<div class="controls">
										<label for="bank_name">Bank Name:</label>
										<?php echo form_input($bank_name, '', 'class="input-xxlarge" placeholder="Bank Name"');?>
									</div>

									<div class="controls">
										<label for="acct_name">Account Name:</label>
										<?php echo form_input($acct_name, '', 'class="input-xxlarge" placeholder="Account Name"');?>
									</div>

									<div class="controls">
										<label for="acct_number">Account Number:</label>
										<?php echo form_input($acct_number, '', 'class="input-xxlarge" placeholder="Account Number"');?>
									</div>
								</div>
							</div>

							<div class="controls well">
								<div class="control-group">
										
									<label> Other Identification: </label>

									<div class="controls">
										<label for="others">Other Identification:</label>
										<?php echo form_textarea($others, '', 'class="input-xxlarge" placeholder="Other Identification"');?>
									</div>

								</div>
							</div>					

					  </div>

						<div class="controls well">
							<label for="password"> Password: </label>
							<?php echo form_input($password, '', 'class="input-xxlarge" placeholder="Password"');?>

							<label for="confirm_password"> Confirm Password: </label>
							<?php echo form_input($password_confirm, '', 'class="input-xxlarge" placeholder="Confirm Password"');?>
						</div>

						<div class="controls clearfix">
							<?php echo form_submit('submit', 'Create User', 'class="btn btn-primary btn-medium"');?>
						</div>	  
					
					</div>

					<!-- END TABS -->

				</div>								

				</div>

				<?php echo form_close();?>		
				<div class="clearfix"></div>
			</div>			
		</div>
	</div>

</div>