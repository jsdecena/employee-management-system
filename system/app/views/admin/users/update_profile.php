<div id="main_content" class="span9">	
	<div class="bs-docs-example pull-left <?php echo $this->uri->segment(2) ?>">
		<div class="controls-group">
			<h2>My Profile</h2>
			<div class="control-group">
				<p>Please enter your information below.</p>
			</div>		

			<?php if ( $message ) : ?>
			
				<div id="infoMessage" class="alert-block alert-success fade in">
					<a class="close" data-dismiss="alert"> <i class="icon-remove">&nbsp;</i></a>
					<?php echo $message;?>
				</div>

			<?php endif; ?>
			
			<?php echo form_open("users/edit_my_profile");?>

			<div class="control-group span8">

				<div class="personal_info">

					<ul class="nav nav-tabs" id="myTab">
					  <li class="active" data-toggle="tab"><a href="javascript: void(0)" data-target="#info">Basic Info.</a></li>
					  <li data-toggle="tab"><a href="javascript: void(0)" data-target="#extended">Extended Info.</a></li>
					</ul>				

					<div class="tab-content">

						<?php //////////////////////////////////////////////////////////////// TAB 1  //////////////////////////////////////////////////////////////// ?>

						<div class="tab-pane active in" id="info">

						<div id="employee_profile">
							
							<div class="pull-right">
								<a href="javascript: void(0)" id="edit_employee_profile_btn" class="btn btn-info edit_prof_btn">Edit</a>
								<?php echo form_submit('save_my_profile', 'Save your details', 'id="save_employee_profile" class="btn btn-primary edit_prof_btn hidden"');?>
							</div>

							<div id="employee_profile_show">

								<label><strong>Employee Name:</strong></label>
								<div class="control-group">
									<p><?php echo $this->data['prefix']['value'] .'. '. $this->data['first_name']['value'] .' '. $this->data['middle_name']['value'] .' '. $this->data['last_name']['value'] ?></p>
								</div>

								<label><strong>Nationality:</strong></label>
								<div class="control-group">
									<p><?php echo $this->data['nationality']['value']; ?></p>
								</div>

								<label><strong>Company Position:</strong></label>
								<div class="control-group">
									<p><?php echo $this->data['position']['value']; ?></p>
								</div>

								<label><strong>Job Scope:</strong></label>
								<div class="control-group">
									<p><?php echo nl2br($this->data['job_scope']['value']); ?></p>
								</div>							
							</div>

							<div id="edit_employee_profile" class="profile hidden">
								<label> Prefix: </label>
								<div class="btn-group">

									<div class="btn-group" data-toggle="buttons-radio">
										<div class="btn">
											<span>Mr.</span> 
											<?php if ( $this->data['prefix']['value'] == 'Mr') : ?>
												<?php echo form_radio('prefix', 'Mr', TRUE ); ?>
											<?php else: ?>
												<?php echo form_radio('prefix', 'Mr' ); ?>
											<?php endif; ?>
										</div>
										<div class="btn">
											<span>Ms.</span>
											<?php if ( $this->data['prefix']['value'] == 'Ms') : ?>
												<?php echo form_radio('prefix', 'Ms', TRUE ); ?>
											<?php else: ?>
												<?php echo form_radio('prefix', 'Ms' ); ?>
											<?php endif; ?>										
										</div>
										<div class="btn">
											<span>Mrs.</span>
											<?php if ( $this->data['prefix']['value'] == 'Mrs') : ?>
												<?php echo form_radio('prefix', 'Mrs', TRUE ); ?>
											<?php else: ?>
												<?php echo form_radio('prefix', 'Mrs' ); ?>
											<?php endif; ?>										
										</div>
									</div>								
								</div>

								<label for="first_name"> First Name: </label>
								<div class="controls">
									<?php echo form_input($first_name, '', 'class="input-xxlarge" placeholder="First Name"');?>
								</div>							

								<label for="middle_name"> Middle Name: </label>
								<div class="controls">
									<?php echo form_input($middle_name, '', 'class="input-xxlarge" placeholder="Middle Name"');?>
								</div>							

								<label for="last_name"> Last Name: </label>
								<div class="controls">
									<?php echo form_input($last_name, '', 'class="input-xxlarge" placeholder="Last Name"');?>
								</div>

								<label for="nationality"> Nationality: </label>
								<div class="controls">
									<?php echo form_input($nationality, '', 'class="input-xxlarge" placeholder="Nationality/Citizenship"');?>									
								</div>

								<label for="position"> Company Position: </label>
								<div class="controls">
									<select name="position" id="position">
										<option value="0">-- Select Position -- </option>
										<option value="Company Owner" <?php if ( $user->position == 'Company Owner') : ?>selected="selected"<?php endif; ?>>Company Owner</option>
										<option value="Company Co-Owner" <?php if ( $user->position == 'Company Co-Owner') : ?>selected="selected"<?php endif; ?>>Company Co-Owner</option>								
										<option value="Web Designer" <?php if ( $user->position == 'Web Designer') : ?>selected="selected"<?php endif; ?>>Web Designer</option>
										<option value="Web Developer" <?php if ( $user->position == 'Web Developer') : ?>selected="selected"<?php endif; ?>>Web Developer</option>
										<option value="Sales Consultant" <?php if ( $user->position == 'Sales Consultant') : ?>selected="selected"<?php endif; ?>>Sales Consultant</option>
										<option value="Admin Assistant" <?php if ( $user->position == 'Admin Assistant') : ?>selected="selected"<?php endif; ?>>HR / Admin</option>
									</select>				
								</div>

								<label for="job_scope"> Job Scope: </label>
								<div class="controls">
									<?php echo form_textarea($job_scope, '', 'class="input-xxlarge" placeholder="Job Scope"');?>
								</div>							
							</div>

							<div class="clearfix"></div>					
						</div>

						<section>
							<div class="pull-right">
								<a href="javascript: void(0)" id="edit_address_btn" class="btn btn-info edit_add_btn">Edit</a>
								<?php echo form_submit('save_my_profile', 'Save your details', 'id="save_address" class="btn btn-primary edit_add_btn hidden"');?>
							</div>						

							<div id="show_address">
								
								<label><strong>Employee Address:</strong> </label>

								<address>
								  <?php echo $this->data['street_addr']['value'] ?><br>
								  <?php echo $this->data['town_city_addr']['value'] ?> <?php echo $this->data['post_code_addr']['value'] ?><br>
								  <?php if ( $this->data['phone']['value'] ) : ?> <abbr title="Phone">P:</abbr> <?php echo $this->data['phone']['value'] . ' <br /> '; endif; ?>
								  <?php if ( $this->data['m_phone']['value'] ) : ?> <abbr title="Mobile Phone">M:</abbr> <?php echo $this->data['m_phone']['value']; endif; ?>							  
								</address>
								
								<address>
								  <strong>Employee's email:</strong><br>
								  <a href="mailto:<?php echo $this->data['email']['value'] ?>"><?php echo $this->data['email']['value'] ?></a>
								</address>
							</div>

							<div id="edit_address" class="control-group address hidden">
								
								<div>
									<div>
										<label for="street_addr">Street Address</label>
										<?php echo form_input($street_addr, '', 'class="input-xxlarge"');?>
									</div>
									<div>
										<label for="town_city_addr">Town / City </label>
										<?php echo form_input($town_city_addr, '', 'class="input-xxlarge"');?>
									</div>
									<div>
										<label for="post_code_addr">Post Code </label>
										<?php echo form_input($post_code_addr, '', 'class="input-xxlarge"');?>
									</div>
									<div>
										<label for="country_addr">Country </label>
										<?php echo form_input($country_addr, '', 'class="input-xxlarge"');?>						
									</div>
									<div>
										<label for="email"> Email: </label>
										<?php echo form_input($email, '', 'class="input-xxlarge" placeholder="Email"');?>
									</div>

									<div>
										<label for="phone"> Phone: </label>
										<?php echo form_input($phone, '', 'class="input-xxlarge" placeholder="Phone"');?>

										<label for="phone"> Mobile Phone: </label>
										<?php echo form_input($m_phone, '', 'class="input-xxlarge" placeholder="Mobile Phone"');?>							
									</div>

								</div>
							</div>
						</section>						
							
						</div>

						<?php //////////////////////////////////////////////////////////////// TAB 2  //////////////////////////////////////////////////////////////// ?>

						<div class="tab-pane" id="extended">

								<div id="more_info_edit">
									<label>Length of Stay: </label>
									<div class="input-prepend">
										<span class="add-on"><i class="icon-calendar"></i></span>
										<?php echo form_input($hired_since, '', 'class="input-xxlarge" readonly');?>					
									</div>								

									<label>Hired Date: </label>
									<div class="input-prepend">
										<span class="add-on"><i class="icon-calendar"></i></span>
										<?php echo form_input($employment_date, '', 'class="input-xxlarge" readonly');?>									
									</div>								

									<label> Monthly Salary: </label>
									<div class="input-prepend">							
										<span class="add-on"><?php echo $this->data['default_currency'] ?></span>
										<?php echo form_input($monthly_salary, '', 'class="input-xxlarge" placeholder="30,000" readonly');?>							
									</div>

									<label>Annual Leaves: </label>
									<div>
										<?php echo form_input($annual_leaves, '', 'class="input-xxlarge" readonly');?>								
									</div>

									<label>Sick Leaves: </label>
									<div>
										<?php echo form_input($sick_leaves, '', 'class="input-xxlarge" readonly');?>								
									</div>
									
									<?php if ( $this->data['prefix']['value'] == 'Mrs' || $this->data['prefix']['value'] == 'Ms') : ?>
										<label>Maternity Leaves: </label>
										<div>
											<?php echo form_input($maternity_leaves, '', 'class="input-xxlarge"');?>								
										</div>
									<?php endif; ?>
								</div>

								<section id="bank_info_edit">
									<div class="control-group">
											<label> Bank Details</label>
										<div>
											<label for="bank_name">Bank Name:</label>
											<div>
												<?php echo form_input($bank_name, '', 'class="input-xxlarge" placeholder="Bank Name"');?>
											</div>										
										</div>

										<div>
											<label for="acct_name">Account Name:</label>
											<div>
												<?php echo form_input($acct_name, '', 'class="input-xxlarge" placeholder="Account Name"');?>
											</div>										
										</div>

										<div>
											<label for="acct_number">Account Number:</label>
											<div>
												<?php echo form_input($acct_number, '', 'class="input-xxlarge" placeholder="Account Number"');?>
											</div>										
										</div>
									</div>
								</section>

								<div id="other_info_edit">

									<div class="control-group">
											
										<label> Other Identification: </label>

										<div>
											<div>
												<?php echo form_textarea($others, '', 'class="input-xxlarge" placeholder="Other Identification"');?>
											</div>										
										</div>

									</div>
								</div>					
						</div>

						<div class="control-group">

								<label for="password"> Password: ( if you changing password ) </label>
								<?php echo form_input($password, '', 'class="input-xxlarge" placeholder="Password"');?>

								<label> Confirm Password: ( if you changing password ) </label>
								<?php echo form_input($password_confirm, '', 'class="input-xxlarge" placeholder="Confirm Password"');?>
							
							<div class="btn-group hidden_inputs">
								<?php echo form_hidden('id', $user->id);?>
								<?php echo form_hidden($csrf); ?>							
								<?php echo form_submit('save_my_profile', 'Save', 'class="btn btn-primary"');?>
								<?php echo anchor( 'users/users_list', 'Cancel', 'class="btn btn-warning"'); ?>					
							</div>
							<div class="clearfix"></div>
						</div>					
					</div>
					
				</div>

			</div>

			<?php echo form_close();?>
			<div class="clearfix"></div>
		</div>
	</div>

</div>