<div id="main_content" class="span9">
	<div class="bs-docs-example">
		<div class="controls">
			<h2>Leave Applications History</h2>
			<div class="control-group">
				<p>This is the leave history of <strong><em><?php echo $user->first_name ." ". $user->last_name; ?></em></strong>.</p>
			</div>			
		</div>
			<?php if ( $this->session->flashdata('success') ) : ?>
				<div class="alert-block alert-success">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>			
			<?php elseif ( $this->session->flashdata('error') ) : ?>
				<div class="alert-block alert-error">
					<button type="button" class="close" data-dismiss="alert">×</button><?php echo $this->session->flashdata('error') ?>
				</div>				
			<?php endif; ?>
			<?php if (isset($user_history)) : ?>
				<table class="table table-striped <?php echo $this->uri->segment(2) ?>">
				  <thead>
					<tr>
						<th>Date Applied</th>
						<th class="hidden-phone">Title</th>
						<th class="hidden-phone">Description</th>
						<th># of Days</th>
						<th class="hidden-phone">Start Date</th>
						<th class="hidden-phone">End Date</th>
						<th class="hidden-phone">Leave Type</th>
						<th class="hidden-phone">Timing</th>
						<th>Status</th>
					</tr>
				  </thead>
			  <tbody>
				<?php foreach ( $user_history as $history ): ?>
					<tr>
						<td><?php echo date("F j, Y", strtotime($history->date_applied)) ?></td>
						<td class="hidden-phone"><?php echo $history->title; ?></td>
						<td class="hidden-phone"><?php echo $history->description; ?></td>
						<td><?php echo $history->no_days; ?></td>
						<td class="hidden-phone"><?php echo $history->start_date; ?></td>
						<td class="hidden-phone"><?php echo $history->end_date; ?></td>
						<td class="hidden-phone"><?php echo $history->leave_type; ?></td>
						<td class="hidden-phone"><?php echo $history->leave_timing; ?></td>
						<td>
							<?php if ($history->status == 0 ) : ?>
								<a href="#" class="btn btn-info">Pending</a>
							<?php elseif ($history->status == 1 ) : ?>
								<a href="#" class="btn btn-success">Approved</a>
							<?php elseif ($history->status == 2) : ?>
								<a href="#" class="btn btn-danger">Denied</a>
							<?php endif; ?>
						</td>
					</tr>
				<?php endforeach; ?>			  	
			  </tbody>
				</table>
			<?php else: ?>
				<section><p class="alert alert-error">You do not have a leave application.</p></section>
			<?php endif; ?>
			<p><?php echo $links; ?></p>
	</div>	
</div>