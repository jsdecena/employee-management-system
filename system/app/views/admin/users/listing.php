<div id="main_content" class="span9">
	<div class="bs-docs-example">
		<div class="controls">
			<h2>Users List</h2>
			<div class="control-group">
				<p>These are the list of employees.</p>
			</div>			
		</div>	

			<?php if ( $this->session->flashdata('success') ) : ?>
				<div class="alert-block alert-success">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>			
			<?php elseif ( $this->session->flashdata('error') ) : ?>
				<div class="alert-block alert-error">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<?php echo $this->session->flashdata('error') ?>
				</div>				
			<?php endif; ?>
			
		<div class="pull-left">
			<a href="<?php echo site_url('users/create');?>" class="btn btn-info">Create a user</a>
		</div>
			<table class="table table-striped <?php echo $this->uri->segment(2) ?>">
			  <thead>
				<tr>
					<th>ID</th>
					<th class="hidden-phone">First Name</th>
					<th class="hidden-phone">Last Name</th>
					<th>Email</th>
					<th class="hidden-phone">Groups</th>
					<th class="status hidden-phone">Status</th>
					<th>Actions</th>
				</tr>
			  </thead>
			  <tbody>
				<?php foreach ($users as $user):?>
					<tr>
						<td><?php echo $user->id; ?></td>
						<td class="hidden-phone"><?php echo $user->first_name;?></td>
						<td class="hidden-phone"><?php echo $user->last_name;?></td>
						<td><?php echo $user->email;?></td>
						<td class="hidden-phone">
							<?php foreach ($user->groups as $group):?>
								<?php echo $group->name;?><br />
			                <?php endforeach?>
						</td>
						<td class="status hidden-phone">
							<?php if ($user->active) : ?>
								<?php echo anchor("auth/deactivate/$user->id", '<i class="icon-ok icon-white">&nbsp;</i>', 'class="btn btn-success"'); ?>
							<?php else : ?>
								<?php echo anchor("auth/activate/$user->id", '<i class="icon-remove icon-white">&nbsp;</i>', 'class="btn btn-danger" onClick="return confirm(\'Activate this user?\')"'); ?>
							<?php endif; ?>
						</td>						
						<td>
							<div class="btn-group">
								<?php echo anchor("users/history/$user->id", 'History', 'class="btn btn-info"'); ?>
								<?php echo anchor("users/edit/$user->id", '<i class="icon-pencil icon-white">&nbsp;</i> Edit', 'class="btn btn-primary btn-medium"'); ?>								
								<?php echo anchor("users/delete/$user->id", '<i class="icon-trash icon-white">&nbsp;</i> Delete', 'class="btn btn-danger" onClick="return confirm(\'Delete this user?\')"'); ?>
							</div>
						</td>
					</tr>
				<?php endforeach;?>
			  </tbody>
			</table>
			<p><?php echo $links; ?></p>
	</div>	
</div>