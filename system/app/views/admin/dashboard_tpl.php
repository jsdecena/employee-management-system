<!doctype html>
<html>
<head>
  <title><?php echo $this->template->title->default("Default title"); ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta charset="utf-8">
  <meta name="description" content="<?php echo $this->template->description; ?>">
  <meta name="author" content="">
  <?php echo $this->template->meta; ?>
  <?php echo $this->template->stylesheet; ?>
  <link rel="shortcut icon" href="<?php echo base_url('assets/ico/favicon.ico'); ?>">
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url('assets/ico/apple-touch-icon-144-precomposed.png'); ?>">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url('assets/ico/apple-touch-icon-114-precomposed.png'); ?>">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url('assets/ico/apple-touch-icon-72-precomposed.png'); ?>">
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url('assets/ico/apple-touch-icon-57-precomposed.png'); ?>">  
</head>
<body>

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?php echo base_url(); ?>"> <?php echo $s_name; ?></a>
          <div class="nav-collapse collapse">
            <ul class="nav">
              <li<?php if ( $this->uri->segment(2) == 'dashboard') : ?> class="active"<?php endif; ?>><a href="<?php echo base_url('auth/dashboard'); ?>">Dashboard</a></li>
              
              <?php if ( $this->ion_auth->is_admin() ) : ?>
                <li <?php if ( $this->uri->segment(1) == 'users' && $this->uri->segment(2) == 'listing' || $this->uri->segment(1) == 'users' && $this->uri->segment(2) == 'edit' || $this->uri->segment(1) == "users" && $this->uri->segment(2) == "create" || $this->uri->segment(1) == "users" && $this->uri->segment(2) == "history") : ?> class="active"<?php endif; ?>><a href="<?php echo site_url('users/listing');?>">Manage Users</a></li>
                <li<?php if ( $this->uri->segment(1) == 'settings') : ?> class="active"<?php endif; ?>> <a href="<?php echo base_url('settings'); ?>">Settings</a></li>
              <?php endif; ?>
              <li><a href="<?php echo base_url('auth/logout'); ?>">Log out</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

  <div class="container">

    <div class="row">

        <div class="span3 sidebar">
          <?php $this->load->view('admin/sidebar'); ?>
        </div>  
        <?php echo $this->template->content; ?>
    </div>

  </div>

  <?php echo $this->template->javascript; ?>
</body>
</html>